/*
 * @brief Minimum bracketing tester.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jun 15, 2017
 * @version 0.5
 */

#include "bisect.h"
#include "core_lapack.h"
#include "func.h"

#include <stdio.h>


int main(int argc, char *argv[]) {

    // Rightsizer parameters
    int range_nb[3] = {64, 128, 256};
    int tol_nb      =  8;

    int exec = 0;

    // PLASMA parameters
    plasma_enum_t transA = PlasmaNoTrans;
    plasma_enum_t transB = PlasmaNoTrans;

    int const m = 10000;
    int const n = 10000;
    int const k = 10000;

    int lda, ldb, ldc;
    int  an,  bn,  cn;

    if (transA == PlasmaNoTrans) {
        lda = imax(1, m);  an = k;
    }
    else {
        lda = imax(1, k);  an = m;
    }

    if (transB == PlasmaNoTrans) {
        ldb = imax(1, k);  bn = n;
    }
    else {
        ldb = imax(1, n);  bn = k;
    }

    ldc = imax(1, m);  cn = n;

    double const EN = 2.7182818284590452;
    double const PI = 3.1415926535897932;

    double alpha = EN;
    double beta  = PI;

    // Allocate memory
    double *A = (double*)malloc((size_t)lda*an*sizeof(double));
    assert(A != NULL);

    double *B = (double*)malloc((size_t)ldb*bn*sizeof(double));
    assert(B != NULL);

    double *C = (double*)malloc((size_t)ldc*cn*sizeof(double));
    assert(C != NULL);

    int seed[] = {0, 0, 0, 1};

    // Initialize matrices
    lapack_int retval = LAPACKE_dlarnv(1, seed, (size_t)lda*an, A);
    assert(retval == 0);

    retval = LAPACKE_dlarnv(1, seed, (size_t)ldb*bn, B);
    assert(retval == 0);

    retval = LAPACKE_dlarnv(1, seed, (size_t)ldc*cn, C);
    assert(retval == 0);

    // Initialize PLASMA
    plasma_init();

    // Set tuning parameters
    plasma_set(PlasmaTuning, PlasmaDisabled);

    int const K = bisect_niter(range_nb[0], range_nb[2], tol_nb);

    hash_t *h = hash_new(K);

    //--------------------------------------------------------------------------
    printf("Testing minimum bracketing functionality...\n");
    //--------------------------------------------------------------------------
    printf("Initial bracket range: %d, %d, %d\n", range_nb[0], range_nb[1], range_nb[2]);
    printf("nb  |  time  |  GFLOP/s\n");

    bracket_min(&range_nb[0], &range_nb[1], &range_nb[2], &tol_nb, h, &exec,
                transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                dgemm_time_nb);

    printf("New bracket range (%d): %d, %d, %d\n", exec, range_nb[0], range_nb[1], range_nb[2]);

    hash_print(h);
    hash_free(h);

    // Finalize PLASMA
    plasma_finalize();

    // Deallocating memory
    free(A);  free(B);  free(C);

    return 0;
}

// EoF: test_bracket_min.c
