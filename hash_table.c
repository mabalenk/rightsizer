/*
 * @brief Implementation of basic hash table.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 22, 2017
 * @version 0.6
 */

#include "plasma.h"
#include "core_lapack.h"
#include "hash_table.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


// Hash table constructor
hash_t *hash_new(int size) {

    hash_t *h = calloc(1,   sizeof(hash_t));

    h->n     = 0;
    h->size  = size;
    h->key   = calloc(size, sizeof(int));
    h->time  = calloc(size, sizeof(double));
    h->flops = calloc(size, sizeof(double));

    return h;
}

// Hash table destructor
void hash_free(hash_t *h) {

    h->n    = 0;
    h->size = 0;
    free(h->key);
    free(h->time);
    free(h->flops);
    free(h);
}

// Index function based on modular hashing
int hash_idx(hash_t *h, int key) {

    // Re-allocate and re-hash table if there is insufficient space
    if (h->n == h->size) {
        hash_rehash(h);
    }

    int i = key % h->size;

    while (h->key[i] && h->key[i] != key)
        i = (i+1) % h->size;

    return i;
}

// Print out hash table
void hash_print(hash_t *h) {

    printf("n|size: %d|%d\n", h->n, h->size);

    for (int i = 0; i < h->size; i++) {
        printf("[%.d]: %.3f %.3f\n", h->key[i], h->time[i], h->flops[i]);
    }
}

// Save hash table into file
void hash_save(hash_t *h, char *fname, char *header) {

    FILE *f = fopen(fname, "w");

    fprintf(f, "%s\n", header);

    for (int i = 0; i < h->size; i++) {

        if (h->key[i] != 0)
            fprintf(f, "%.d\t%.3f\t%.3f\n", h->key[i], h->time[i], h->flops[i]);
    }

    fclose(f);
}

// Element lookup function
double hash_lookup(hash_t *h, int key, char type) {

    int i = hash_idx(h, key);

    return (type == 'f') ? h->flops[i] : h->time[i];
}

// Re-allocate and re-hash table if there is insufficient space
void hash_rehash(hash_t *h) {

    // Create new hash table (double size)
    hash_t *g = hash_new(2*(h->size));

    // Re-hash key|value|value triplets and insert them into new table
    for (int i = 0; i < h->size; i++) {
        hash_ins(g, h->key[i], h->time[i], h->flops[i]);
    }

    // Destroy full hash table
    free(h->key);
    free(h->time);
    free(h->flops);
    h->size = 0;
    h->n    = 0;

    // Swap hash table pointers
    *h = *g;
}

// Element insertion function
void hash_ins(hash_t *h, int key, double time, double flops) {

    double eps = LAPACKE_dlamch('E');

    int    idx      = hash_idx(h, key);
    double currTime = hash_lookup(h, key, 't');

    // Insert new values
    if (fabs(currTime-0.0) <= eps) {

        h->key[idx]   = key;
        h->time[idx]  = time;
        h->flops[idx] = flops;
        h->n++;
    }
    // Replace current values
    else if (time < currTime) {

        h->time[idx]  = time;
        h->flops[idx] = flops;
    }
}

// EoF: hash_table.c
