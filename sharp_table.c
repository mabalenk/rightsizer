/*
 * @brief Implementation of sharp table: table using hash function to create 'unique' key.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 25, 2017
 * @version 0.3
 */

#include "plasma.h"
#include "core_lapack.h"
#include "sharp_table.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// Sharp table constructor
sharp_t *sharp_new(int size) {

    sharp_t *s = calloc(1,   sizeof(sharp_t));

    s->n     = 0;                             // no. of elements
    s->size  = size;                          // initial size of sharp table
    s->key   = calloc(size, sizeof(int));     // key of data entry
    s->nb    = calloc(size, sizeof(int));     // tile size 'nb'
    s->nthr  = calloc(size, sizeof(int));     // no. of OpenMP threads 'nthr'
    s->time  = calloc(size, sizeof(double));  // time in seconds
    s->flops = calloc(size, sizeof(double));  // GFLOP/s

    return s;
}

// Sharp table destructor
void sharp_free(sharp_t *s) {

    s->n    = 0;
    s->size = 0;
    free(s->key);
    free(s->nb);
    free(s->nthr);
    free(s->time);
    free(s->flops);
    free(s);
}

// Generate key using hash function (BSD checksum)
int gen_key(int nb, int nthr) {

    char str[32];

    sprintf(str, "%d,%d", nb, nthr);

    int ch;       // character read
    int key = 0;  // checksum mod 2^16

    for (int i = 0; i <= strlen(str); i++) {

        key = (key >> 1) + ((key & 1) << 15);
        key += str[i];
        key &= 0xffff;  // keep checksum within bounds

    }

    return key;
}

// Index function based on modular hashing
int sharp_idx(sharp_t *s, int nb, int nthr) {

    int key = gen_key(nb, nthr);

    // Re-allocate and re-hash table if there is insufficient space
    if (s->n == s->size) {
        sharp_rehash(s);
    }

    int i = key % s->size;

    while (s->key[i] && s->key[i] != key)
        i = (i+1) % s->size;

    return i;
}

// Print out sharp table
void sharp_print(sharp_t *s) {

    printf("n|size: %d|%d\n", s->n, s->size);

    for (int i = 0; i < s->size; i++) {
        printf("[%d]: %d %d %.3f %.3f\n",
                s->key[i], s->nb[i], s->nthr[i], s->time[i], s->flops[i]);
    }
}

// Save sharp table into file
void sharp_save(sharp_t *s, char *fname, char *header) {

    FILE *f = fopen(fname, "w");

    fprintf(f, "%s\n", header);

    for (int i = 0; i < s->size; i++) {

        if (s->key[i] != 0)
            fprintf(f, "%d\t%d\t%.3f\t%.3f\n",
                    s->nb[i], s->nthr[i], s->time[i], s->flops[i]);
    }

    fclose(f);
}

// Element lookup function
double sharp_lookup(sharp_t *s, int nb, int nthr, char type) {

    int i = sharp_idx(s, nb, nthr);

    return (type == 'f') ? s->flops[i] : s->time[i];
}

// Re-allocate and re-hash table if there is insufficient space
void sharp_rehash(sharp_t *s) {

    // Create new sharp table (double size)
    sharp_t *r = sharp_new(2*(s->size));

    // Re-hash data entries and insert them into new table
    for (int i = 0; i < s->size; i++) {
        sharp_ins(r, s->nb[i], s->nthr[i], s->time[i], s->flops[i]);
    }

    // Destroy full sharp table
    free(s->key);
    free(s->nb);
    free(s->nthr);
    free(s->time);
    free(s->flops);
    s->size = 0;
    s->n    = 0;

    // Swap sharp table pointers
    *s = *r;
}

// Element insertion function
void sharp_ins(sharp_t *s, int nb, int nthr, double time, double flops) {

    double eps = LAPACKE_dlamch('E');

    int    idx      = sharp_idx(s, nb, nthr);
    double currTime = sharp_lookup(s, nb, nthr, 't');

    // Insert new values
    if (fabs(currTime-0.0) <= eps) {

        s->key[idx]   = gen_key(nb, nthr);
        s->nb[idx]    = nb;
        s->nthr[idx]  = nthr;
        s->time[idx]  = time;
        s->flops[idx] = flops;
        s->n++;
    }
    // Replace current values
    else if (time < currTime) {

        s->time[idx]  = time;
        s->flops[idx] = flops;
    }
}

// EoF: sharp_table.c
