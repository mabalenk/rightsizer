/*
 * @brief Prototype implementation of Nelder-Mead optmisation method.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 25, 2017
 * @version 0.8
 */

#include "test.h"
#include "plasma.h"
#include "core_lapack.h"
#include "bisect.h"
#include "nelder_mead.h"

#include <stdio.h>
#include <math.h>


// Prints out vertex coordinates and function value
void print_vertex(char *str, vertex_t v) {

    printf("%s (%.3f,%.3f): %.3f\n", str, v.x, v.y, v.f);
}


// Identifies, if function value for z1 is best, good or worst amongst set of function values for vertices z{1,2,3}
int bgw(vertex_t z1, vertex_t z2, vertex_t z3, vertex_t **b, vertex_t **g, vertex_t **w) {
    
    // best
    if ((z1.f < z2.f) && (z1.f < z3.f)) {
        **b = z1;
    }
    // worst
    else if ((z1.f > z2.f) && (z1.f > z3.f)) {
        **w = z1;
    }
    // good
    else {
        **g = z1;
    }

    return 0;
}


// Re-orders vertices to identify best, good and worst according to function values
int reorder(vertex_t *b, vertex_t *g, vertex_t *w) {

    vertex_t z1 = *b;
    vertex_t z2 = *g;
    vertex_t z3 = *w;

    bgw(z1, z2, z3, &b, &g, &w);
    bgw(z2, z1, z3, &b, &g, &w);
    bgw(z3, z1, z2, &b, &g, &w);

    // @test
    print_vertex("best",  *b);
    print_vertex("good",  *g);
    print_vertex("worst", *w);

    return 0;
}


// Calculates midpoint of good side
int midpoint(vertex_t *b, vertex_t *g, vertex_t *m,
             int (*func)(double, double, double*)) {

    m->x = (b->x + g->x)/2.0;
    m->y = (b->y + g->y)/2.0;

    int status = (*func)(m->x, m->y, &m->f);

    // @test
    print_vertex("midpoint", *m);

    return status;
}


// Calculates reflection point r
int reflect(vertex_t *m, vertex_t *w, vertex_t *r,
            int (*func)(double, double, double*)) {

    r->x = 2.0*m->x - w->x;
    r->y = 2.0*m->y - w->y;

    int status = (*func)(r->x, r->y, &r->f);

    // @test
    print_vertex("reflection", *r);

    return status;
}


// Calculates expansion point e
int expand(vertex_t *r, vertex_t *m, vertex_t *e,
           int (*func)(double, double, double*)) {

    e->x = 2.0*r->x - m->x;
    e->y = 2.0*r->y - m->y;

    int status = (*func)(e->x, e->y, &e->f);

    // @test
    print_vertex("expansion", *e);

    return status;
}


// Calculates contraction point c
int contract(vertex_t *w, vertex_t *m, vertex_t *r, vertex_t *c,
             int (*func)(double, double, double*)) {

    vertex_t c1 = (vertex_t){.x = 0.0, .y = 0.0, .f = 0.0};
    vertex_t c2 = (vertex_t){.x = 0.0, .y = 0.0, .f = 0.0};

    int status1 = midpoint(w, m, &c1, func);
    int status2 = midpoint(m, r, &c2, func);

    *c = (c1.f < c2.f) ? c1 : c2;

    // @test
    print_vertex("contraction", *c);

    return status1 && status2;
}


// Calculates shrinking point s
int shrink(vertex_t *b, vertex_t *w, vertex_t *s,
           int (*func)(double, double, double*)) {

    int status = midpoint(b, w, s, func);

    // @test
    print_vertex("shrinking", *s);

    return status;
}


int nelder_mead(vertex_t *b, vertex_t *g, vertex_t *w, int *exec, int *iter,
                int (*func)(double, double, double*)) {

    double eps = LAPACKE_dlamch('E');
    double tol = sqrt(eps);

    *exec = 0;  *iter = 0;

    // Define auxiliary vertices
    vertex_t m, r, e, c, s;

    // Obtain initial triangle BGW
    int status = (*func)(b->x, b->y, &b->f);  *exec += 1;
        status = (*func)(g->x, g->y, &g->f);  *exec += 1;
        status = (*func)(w->x, w->y, &w->f);  *exec += 1;

    do {

        // Re-order function values
        status = reorder(b, g, w);
    
        // Calculate midpoint of good side
        status = midpoint(b, g, &m, func);  *exec += 1;
    
        // Reflection using point r
        status = reflect(&m, w, &r, func);  *exec += 1;
    
        // Reflect or extend
        if (r.f < g->f) {
    
            // Replace W with R
            if (b->f < r.f)
                *w = r;
            else {
    
                // Expand using point e
                status = expand(&m, &r, &e, func);  *exec += 1;
    
                *w = (e.f < b->f) ? e : r;
            }
        }
        // Contract or shrink
        else {
    
            if (r.f < w->f) *w = r;
    
            // Contract using point c
            status = contract(w, &m, &r, &c, func);  *exec += 2;
    
            if (c.f < w->f)
                *w = c;
            else {
                // Shrink towards b
                status = shrink(b, w, &s, func);  *exec += 1;
    
                *w = s;  *g = m;
            }
        }

        *iter += 1;

    } while (fabs(b->f - g->f) > tol);

    return status;
}

// -----------------------------------------------------------------------------

// Prints out vertex coordinates and function value
void print_vert(char *str, vert_t v) {

    printf("%s (%d,%d): %.3f, %.3f\n", str, v.x, v.y, v.f, v.g);
}


// Prints out experiment results
void print_rslt(vert_t v) {

    printf("%d\t%d\t%.3f\t%.3f\n", v.x, v.y, v.f, v.g);
}


// Identifies, if function value for z1 is best, good or worst amongst set of function values for vertices z{1,2,3}
int bgw_vert(vert_t z1, vert_t z2, vert_t z3, vert_t **b, vert_t **g, vert_t **w) {
    
    // best
    if ((z1.f < z2.f) && (z1.f < z3.f)) {
        **b = z1;
    }
    // worst
    else if ((z1.f > z2.f) && (z1.f > z3.f)) {
        **w = z1;
    }
    // good
    else {
        **g = z1;
    }

    return 0;
}


// Re-orders vertices to identify best, good and worst according to function values
int reorder_vert(vert_t *b, vert_t *g, vert_t *w) {

    vert_t z1 = *b;
    vert_t z2 = *g;
    vert_t z3 = *w;

    bgw_vert(z1, z2, z3, &b, &g, &w);
    bgw_vert(z2, z1, z3, &b, &g, &w);
    bgw_vert(z3, z1, z2, &b, &g, &w);

    // @test
    print_vert("best",  *b);
    print_vert("good",  *g);
    print_vert("worst", *w);

    return 0;
}


// Calculates midpoint of good side
int midpoint_dgemm(vert_t *b, vert_t *g, vert_t *d, int *tol_nb, int *tol_nthr,
                   sharp_t *st, int *exec, double eps,
                   plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                   double alpha, double *A, int lda, double *B, int ldb,
                   double beta, double *C, int ldc,
                   int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                     double*, int, double*, int, double, double*, int,
                                     int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter midpoint\n");

    d->x = nm((b->x + g->x)/2, *tol_nb);
    d->y = nm((b->y + g->y)/2, *tol_nthr);

    d->f = sharp_lookup(st, d->x, d->y, 't');
    d->g = sharp_lookup(st, d->x, d->y, 'f');

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(d->f - 0.0) <= eps) {

        // @test
        // printf("Execute dgemm\n");

        status = (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                               d->x, d->y, &d->f, &d->g);  *exec += 1;

        // @test
        // printf("Store dgemm result\n");

        // Store data entry in sharp table
        sharp_ins(st, d->x, d->y, d->f, d->g);

        // @test
        // printf("Print dgemm result\n");

        print_rslt(*d);
    }

    // @test
    print_vert("midpoint", *d);

    // @test
    // printf("Exit midpoint\n");

    return status;
}


// Calculates reflection point r
int reflect_dgemm(vert_t *d, vert_t *w, vert_t *r, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                  double alpha, double *A, int lda, double *B, int ldb,
                  double beta, double *C, int ldc,
                  int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                    double*, int, double*, int, double, double*, int,
                                    int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter reflect\n");

    r->x = nm(2*d->x - w->x, *tol_nb);
    r->y = nm(2*d->y - w->y, *tol_nthr);

    r->f = sharp_lookup(st, r->x, r->y, 't');
    r->g = sharp_lookup(st, r->x, r->y, 'f');

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(r->f - 0.0) <= eps) {

        // @test
        // printf("Execute dgemm\n");

        status = (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                               r->x, r->y, &r->f, &r->g);  *exec += 1;

        // @test
        // printf("Store dgemm result\n");

        // Store data entry in sharp table
        sharp_ins(st, r->x, r->y, r->f, r->g);

        // @test
        // printf("Print dgemm result\n");

        print_rslt(*r);
    }

    // @test
    print_vert("reflection", *r);

    // @test
    // printf("Exit reflect\n");

    return status;
}


// Calculates expansion point e
int expand_dgemm(vert_t *r, vert_t *d, vert_t *e, int *tol_nb, int *tol_nthr,
                 sharp_t *st, int *exec, double eps,
                 plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                 double alpha, double *A, int lda, double *B, int ldb,
                 double beta, double *C, int ldc,
                 int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                   double*, int, double*, int, double, double*, int,
                                   int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter expand\n");

    e->x = nm(2*r->x - d->x, *tol_nb);
    e->y = nm(2*r->y - d->y, *tol_nthr);

    e->f = sharp_lookup(st, e->x, e->y, 't');
    e->g = sharp_lookup(st, e->x, e->y, 'f');

    // @test
    // print_vert("pre-expansion", *e);

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(e->f - 0.0) <= eps) {

        // @test
        // printf("Execute dgemm\n");

        status = (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                               e->x, e->y, &e->f, &e->g);  *exec += 1;

        // @test
        // printf("Store dgemm result\n");

        // Store data entry in sharp table
        sharp_ins(st, e->x, e->y, e->f, e->g);

        // @test
        // printf("Print dgemm result\n");

        print_rslt(*e);
    }

    // @test
    print_vert("expansion", *e);

    // @test
    // printf("Exit expand\n");

    return status;
}


// Calculates contraction point c
int contract_dgemm(vert_t *w, vert_t *d, vert_t *r, vert_t *c, int *tol_nb, int *tol_nthr,
                   sharp_t *st, int *exec, double eps,
                   plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                   double alpha, double *A, int lda, double *B, int ldb,
                   double beta, double *C, int ldc,
                   int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                     double*, int, double*, int, double, double*, int,
                                     int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter contract\n");

    vert_t c1 = (vert_t){.x = 0, .y = 0, .f = 0.0, .g = 0.0};
    vert_t c2 = (vert_t){.x = 0, .y = 0, .f = 0.0, .g = 0.0};

    int status1 = midpoint_dgemm(w, d, &c1, tol_nb, tol_nthr, st, exec, eps,
                                 transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                                 dgemm_time);

    int status2 = midpoint_dgemm(d, r, &c2, tol_nb, tol_nthr, st, exec, eps,
                                 transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                                 dgemm_time);

    *c = (c1.f < c2.f) ? c1 : c2;

    // @test
    print_vert("contraction", *c);

    // @test
    // printf("Exit contract\n");

    return status1 && status2;
}


// Calculates shrinking point s
int shrink_dgemm(vert_t *b, vert_t *w, vert_t *s, int *tol_nb, int *tol_nthr,
                 sharp_t *st, int *exec, double eps,
                 plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                 double alpha, double *A, int lda, double *B, int ldb,
                 double beta, double *C, int ldc,
                 int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                   double*, int, double*, int, double, double*, int,
                                   int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter shrink\n");

    int status = midpoint_dgemm(b, w, s, tol_nb, tol_nthr, st, exec, eps,
                                transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                                dgemm_time);
    // @test
    print_vert("shrinking", *s);

    // @test
    // printf("Exit shrink\n");

    return status;
}


/*
 * Optimises matrix multiplication routine DGEMM for best tile size 'nb' and
 * no. of OpenMP threads 'nthr' using Nelder-Mead algorithm
 */
int nelder_mead_dgemm(vert_t *b, vert_t *g, vert_t *w,
                      int *tol_nb, int *tol_nthr, int *exec, int *iter, char *fname, char *header,
                      plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                      double alpha, double *A, int lda, double *B, int ldb,
                      double beta, double *C, int ldc,
                      int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                        double*, int, double*, int, double, double*, int,
                                        int, int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    *exec = 0;  *iter = 0;

    // Define auxiliary vertices
    vert_t d, r, e, c, s;

    // Calculate size of sharp table (no. of function executions)
    int const K = 2*bisect_niter(b->x, w->x, *tol_nb) + 3;

    // Create sharp table to store data entries
    sharp_t *st = sharp_new(K);

    // Obtain initial triangle BGW
    int status = (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                               b->x, b->y, &b->f, &b->g);  *exec += 1;

        status = (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                               g->x, g->y, &g->f, &g->g);  *exec += 1;

        status = (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                               w->x, w->y, &w->f, &w->g);  *exec += 1;

        // Store data entries in sharp table
        sharp_ins(st, b->x, b->y, b->f, b->g);
        sharp_ins(st, g->x, g->y, g->f, g->g);
        sharp_ins(st, w->x, w->y, w->f, w->g);

        print_rslt(*b);  print_rslt(*g);  print_rslt(*w);

    do {
        // Re-order function values
        status = reorder_vert(b, g, w);
    
        // Calculate midpoint of good side
        status = midpoint_dgemm(b, g, &d, tol_nb, tol_nthr, st, exec, eps,
                                transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                                dgemm_time);

        // Reflection using point r
        status = reflect_dgemm(&d, w, &r, tol_nb, tol_nthr, st, exec, eps,
                               transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                               dgemm_time);

        // Reflect or extend
        if (r.f < g->f) {
    
            // Replace W with R
            if (b->f < r.f)
                *w = r;
            else {
    
                // Expand using point e
                status = expand_dgemm(&d, &r, &e, tol_nb, tol_nthr, st, exec, eps,
                                      transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                                      dgemm_time);

                *w = (e.f < b->f) ? e : r;
            }
        }
        // Contract or shrink
        else {
    
            if (r.f < w->f) *w = r;
    
            // Contract using point c
            status = contract_dgemm(w, &d, &r, &c, tol_nb, tol_nthr, st, exec, eps,
                                    transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                                    dgemm_time);
            if (c.f < w->f)
                *w = c;
            else {
                // Shrink towards b
                status = shrink_dgemm(b, w, &s, tol_nb, tol_nthr, st, exec, eps,
                                      transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                                      dgemm_time);
                *w = s;  *g = d;
            }
        }

        *iter += 1;

    } while (abs(b->x - g->x) > *tol_nb &&
             abs(b->y - g->y) > *tol_nthr);

    // Print out full sharp table
    sharp_print(st);

    // Save full sharp table into file
    sharp_save(st, fname, header);

    // Destroy sharp table
    sharp_free(st);

    return status;
}

// -----------------------------------------------------------------------------

// Calculates midpoint of good side
int midpoint_dpotrf(vert_t *b, vert_t *g, vert_t *d, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    plasma_enum_t uplo, int n, double *A, int lda,
                    int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                       int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter midpoint\n");

    d->x = nm((b->x + g->x)/2, *tol_nb);
    d->y = nm((b->y + g->y)/2, *tol_nthr);

    d->f = sharp_lookup(st, d->x, d->y, 't');
    d->g = sharp_lookup(st, d->x, d->y, 'f');

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(d->f - 0.0) <= eps) {

        // @test
        // printf("Execute dpotrf\n");

        status = (*dpotrf_time)(uplo, n, A, lda,
                                d->x, d->y, &d->f, &d->g);  *exec += 1;

        // @test
        // printf("Store dpotrf result\n");

        // Store data entry in sharp table
        sharp_ins(st, d->x, d->y, d->f, d->g);

        // @test
        // printf("Print dpotrf result\n");

        print_rslt(*d);
    }

    // @test
    print_vert("midpoint", *d);

    // @test
    // printf("Exit midpoint\n");

    return status;
}


// Calculates reflection point r
int reflect_dpotrf(vert_t *d, vert_t *w, vert_t *r, int *tol_nb, int *tol_nthr,
                   sharp_t *st, int *exec, double eps,
                   plasma_enum_t uplo, int n, double *A, int lda,
                   int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                      int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter reflect\n");

    r->x = nm(2*d->x - w->x, *tol_nb);
    r->y = nm(2*d->y - w->y, *tol_nthr);

    // printf("d.y: %d, w.y: %d, tol: %d\n", d->y, w->y, *tol_nthr);
    // printf("2*d.y - w.y: %d\n", 2*d->y - w->y);

    r->f = sharp_lookup(st, r->x, r->y, 't');
    r->g = sharp_lookup(st, r->x, r->y, 'f');

    // @test
    // print_vert("pre-reflection", *r);

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(r->f - 0.0) <= eps) {

        // @test
        // printf("Execute dpotrf\n");

        status = (*dpotrf_time)(uplo, n, A, lda,
                                r->x, r->y, &r->f, &r->g);  *exec += 1;

        // @test
        // printf("Store dpotrf result\n");

        // Store data entry in sharp table
        sharp_ins(st, r->x, r->y, r->f, r->g);

        // @test
        // printf("Print dpotrf result\n");

        print_rslt(*r);
    }

    // @test
    print_vert("reflection", *r);

    // @test
    // printf("Exit reflect\n");

    return status;
}


// Calculates expansion point e
int expand_dpotrf(vert_t *r, vert_t *d, vert_t *e, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  plasma_enum_t uplo, int n, double *A, int lda,
                  int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                     int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter expand\n");

    e->x = nm(2*r->x - d->x, *tol_nb);
    e->y = nm(2*r->y - d->y, *tol_nthr);

    // printf("r.y: %d, d.y: %d, tol: %d\n", r->y, d->y, *tol_nthr);
    // printf("2*r.y - d.y: %d\n", 2*r->y - d->y);

    e->f = sharp_lookup(st, e->x, e->y, 't');
    e->g = sharp_lookup(st, e->x, e->y, 'f');

    // @test
    // print_vert("pre-expansion", *e);

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(e->f - 0.0) <= eps) {

        // @test
        // printf("Execute dpotrf\n");

        status = (*dpotrf_time)(uplo, n, A, lda,
                                e->x, e->y, &e->f, &e->g);  *exec += 1;

        // @test
        // printf("Store dpotrf result\n");

        // Store data entry in sharp table
        sharp_ins(st, e->x, e->y, e->f, e->g);

        // @test
        // printf("Print dpotrf result\n");

        print_rslt(*e);
    }

    // @test
    print_vert("expansion", *e);

    // @test
    // printf("Exit expand\n");

    return status;
}


// Calculates contraction point c
int contract_dpotrf(vert_t *w, vert_t *d, vert_t *r, vert_t *c, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    plasma_enum_t uplo, int n, double *A, int lda,
                    int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                       int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter contract\n");

    vert_t c1 = (vert_t){.x = 0, .y = 0, .f = 0.0, .g = 0.0};
    vert_t c2 = (vert_t){.x = 0, .y = 0, .f = 0.0, .g = 0.0};

    int status1 = midpoint_dpotrf(w, d, &c1, tol_nb, tol_nthr, st, exec, eps,
                                  uplo, n, A, lda,
                                  dpotrf_time);

    int status2 = midpoint_dpotrf(d, r, &c2, tol_nb, tol_nthr, st, exec, eps,
                                  uplo, n, A, lda,
                                  dpotrf_time);

    *c = (c1.f < c2.f) ? c1 : c2;

    // @test
    print_vert("contraction", *c);

    // @test
    // printf("Exit contract\n");

    return status1 && status2;
}


// Calculates shrinking point s
int shrink_dpotrf(vert_t *b, vert_t *w, vert_t *s, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  plasma_enum_t uplo, int n, double *A, int lda,
                  int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                     int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter shrink\n");

    int status = midpoint_dpotrf(b, w, s, tol_nb, tol_nthr, st, exec, eps,
                                 uplo, n, A, lda,
                                 dpotrf_time);
    // @test
    print_vert("shrinking", *s);

    // @test
    // printf("Exit shrink\n");

    return status;
}


/*
 * Optimises Cholesky factorisation routine DPOTRF for best tile size 'nb' and
 * no. of OpenMP threads 'nthr' using Nelder-Mead algorithm
 */
int nelder_mead_dpotrf(vert_t *b, vert_t *g, vert_t *w,
                       int *tol_nb, int *tol_nthr, int *exec, int *iter, char *fname, char *header,
                       plasma_enum_t uplo, int n, double *A, int lda,
                       int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                          int, int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    *exec = 0;  *iter = 0;

    // Define auxiliary vertices
    vert_t d, r, e, c, s;

    // Calculate size of sharp table (no. of function executions)
    int const K = 2*bisect_niter(b->x, w->x, *tol_nb) + 3;

    // Create sharp table to store data entries
    sharp_t *st = sharp_new(K);

    // Obtain initial triangle BGW
    int status = (*dpotrf_time)(uplo, n, A, lda,
                                b->x, b->y, &b->f, &b->g);  *exec += 1;

        status = (*dpotrf_time)(uplo, n, A, lda,
                                g->x, g->y, &g->f, &g->g);  *exec += 1;

        status = (*dpotrf_time)(uplo, n, A, lda,
                                w->x, w->y, &w->f, &w->g);  *exec += 1;

        // Store data entries in sharp table
        sharp_ins(st, b->x, b->y, b->f, b->g);
        sharp_ins(st, g->x, g->y, g->f, g->g);
        sharp_ins(st, w->x, w->y, w->f, w->g);

        print_rslt(*b);  print_rslt(*g);  print_rslt(*w);

    do {
        // Re-order function values
        status = reorder_vert(b, g, w);
    
        // Calculate midpoint of good side
        status = midpoint_dpotrf(b, g, &d, tol_nb, tol_nthr, st, exec, eps,
                                 uplo, n, A, lda,
                                 dpotrf_time);

        // Reflection using point r
        status = reflect_dpotrf(&d, w, &r, tol_nb, tol_nthr, st, exec, eps,
                                uplo, n, A, lda,
                                dpotrf_time);

        // Reflect or extend
        if (r.f < g->f) {
    
            // Replace W with R
            if (b->f < r.f)
                *w = r;
            else {
    
                // Expand using point e
                status = expand_dpotrf(&d, &r, &e, tol_nb, tol_nthr, st, exec, eps,
                                       uplo, n, A, lda,
                                       dpotrf_time);

                *w = (e.f < b->f) ? e : r;
            }
        }
        // Contract or shrink
        else {
    
            if (r.f < w->f) *w = r;
    
            // Contract using point c
            status = contract_dpotrf(w, &d, &r, &c, tol_nb, tol_nthr, st, exec, eps,
                                     uplo, n, A, lda,
                                     dpotrf_time);
            if (c.f < w->f)
                *w = c;
            else {
                // Shrink towards b
                status = shrink_dpotrf(b, w, &s, tol_nb, tol_nthr, st, exec, eps,
                                       uplo, n, A, lda,
                                       dpotrf_time);
                *w = s;  *g = d;
            }
        }

        *iter += 1;

    } while (abs(b->x - g->x) > *tol_nb &&
             abs(b->y - g->y) > *tol_nthr);

    // Print out full sharp table
    sharp_print(st);

    // Save full sharp table into file
    sharp_save(st, fname, header);

    // Destroy sharp table
    sharp_free(st);

    return status;
}

// -----------------------------------------------------------------------------

// Calculates midpoint of good side
int midpoint_dgetrf(vert_t *b, vert_t *g, vert_t *d, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    int m, int n, double *A, int lda, int *iPiv,
                    int (*dgetrf_time)(int, int, double*, int, int*,
                                       int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter midpoint\n");

    d->x = nm((b->x + g->x)/2, *tol_nb);
    d->y = nm((b->y + g->y)/2, *tol_nthr);

    d->f = sharp_lookup(st, d->x, d->y, 't');
    d->g = sharp_lookup(st, d->x, d->y, 'f');

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(d->f - 0.0) <= eps) {

        // @test
        // printf("Execute dgetrf\n");

        status = (*dgetrf_time)(m, n, A, lda, iPiv,
                                d->x, d->y, &d->f, &d->g);  *exec += 1;

        // @test
        // printf("Store dgetrf result\n");

        // Store data entry in sharp table
        sharp_ins(st, d->x, d->y, d->f, d->g);

        // @test
        // printf("Print dgetrf result\n");

        print_rslt(*d);
    }

    // @test
    print_vert("midpoint", *d);

    // @test
    // printf("Exit midpoint\n");

    return status;
}


// Calculates reflection point r
int reflect_dgetrf(vert_t *d, vert_t *w, vert_t *r, int *tol_nb, int *tol_nthr,
                   sharp_t *st, int *exec, double eps,
                   int m, int n, double *A, int lda, int *iPiv,
                   int (*dgetrf_time)(int, int, double*, int, int*,
                                      int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter reflect\n");

    r->x = nm(2*d->x - w->x, *tol_nb);
    r->y = nm(2*d->y - w->y, *tol_nthr);

    r->f = sharp_lookup(st, r->x, r->y, 't');
    r->g = sharp_lookup(st, r->x, r->y, 'f');

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(r->f - 0.0) <= eps) {

        // @test
        // printf("Execute dgetrf\n");

        status = (*dgetrf_time)(m, n, A, lda, iPiv,
                                r->x, r->y, &r->f, &r->g);  *exec += 1;

        // @test
        // printf("Store dgetrf result\n");

        // Store data entry in sharp table
        sharp_ins(st, r->x, r->y, r->f, r->g);

        // @test
        // printf("Print dgetrf result\n");

        print_rslt(*r);
    }

    // @test
    print_vert("reflection", *r);

    // @test
    // printf("Exit reflect\n");

    return status;
}


// Calculates expansion point e
int expand_dgetrf(vert_t *r, vert_t *d, vert_t *e, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  int m, int n, double *A, int lda, int *iPiv,
                  int (*dgetrf_time)(int, int, double*, int, int*,
                                     int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter expand\n");

    e->x = nm(2*r->x - d->x, *tol_nb);
    e->y = nm(2*r->y - d->y, *tol_nthr);

    e->f = sharp_lookup(st, e->x, e->y, 't');
    e->g = sharp_lookup(st, e->x, e->y, 'f');

    // @test
    // print_vert("pre-expansion", *e);

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(e->f - 0.0) <= eps) {

        // @test
        // printf("Execute dgetrf\n");

        status = (*dgetrf_time)(m, n, A, lda, iPiv,
                                e->x, e->y, &e->f, &e->g);  *exec += 1;

        // @test
        // printf("Store dgetrf result\n");

        // Store data entry in sharp table
        sharp_ins(st, e->x, e->y, e->f, e->g);

        // @test
        // printf("Print dgetrf result\n");

        print_rslt(*e);
    }

    // @test
    print_vert("expansion", *e);

    // @test
    // printf("Exit expand\n");

    return status;
}


// Calculates contraction point c
int contract_dgetrf(vert_t *w, vert_t *d, vert_t *r, vert_t *c, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    int m, int n, double *A, int lda, int *iPiv,
                    int (*dgetrf_time)(int, int, double*, int, int*,
                                       int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter contract\n");

    vert_t c1 = (vert_t){.x = 0, .y = 0, .f = 0.0, .g = 0.0};
    vert_t c2 = (vert_t){.x = 0, .y = 0, .f = 0.0, .g = 0.0};

    int status1 = midpoint_dgetrf(w, d, &c1, tol_nb, tol_nthr, st, exec, eps,
                                  m, n, A, lda, iPiv,
                                  dgetrf_time);

    int status2 = midpoint_dgetrf(d, r, &c2, tol_nb, tol_nthr, st, exec, eps,
                                  m, n, A, lda, iPiv,
                                  dgetrf_time);

    *c = (c1.f < c2.f) ? c1 : c2;

    // @test
    print_vert("contraction", *c);

    // @test
    // printf("Exit contract\n");

    return status1 && status2;
}


// Calculates shrinking point s
int shrink_dgetrf(vert_t *b, vert_t *w, vert_t *s, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  int m, int n, double *A, int lda, int *iPiv,
                  int (*dgetrf_time)(int, int, double*, int, int*,
                                     int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter shrink\n");

    int status = midpoint_dgetrf(b, w, s, tol_nb, tol_nthr, st, exec, eps,
                                 m, n, A, lda, iPiv,
                                 dgetrf_time);
    // @test
    print_vert("shrinking", *s);

    // @test
    // printf("Exit shrink\n");

    return status;
}


/*
 * Optimises LU factorisation routine DGETRF for best tile size 'nb' and
 * no. of OpenMP threads 'nthr' using Nelder-Mead algorithm
 */
int nelder_mead_dgetrf(vert_t *b, vert_t *g, vert_t *w,
                       int *tol_nb, int *tol_nthr, int *exec, int *iter, char *fname, char *header,
                       int m, int n, double *A, int lda, int *iPiv,
                       int (*dgetrf_time)(int, int, double*, int, int*,
                                          int, int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    *exec = 0;  *iter = 0;

    // Define auxiliary vertices
    vert_t d, r, e, c, s;

    // Calculate size of sharp table (no. of function executions)
    int const K = 2*bisect_niter(b->x, w->x, *tol_nb) + 3;

    // Create sharp table to store data entries
    sharp_t *st = sharp_new(K);

    // Obtain initial triangle BGW
    int status = (*dgetrf_time)(m, n, A, lda, iPiv,
                                b->x, b->y, &b->f, &b->g);  *exec += 1;

        status = (*dgetrf_time)(m, n, A, lda, iPiv,
                                g->x, g->y, &g->f, &g->g);  *exec += 1;

        status = (*dgetrf_time)(m, n, A, lda, iPiv,
                                w->x, w->y, &w->f, &w->g);  *exec += 1;

        // Store data entries in sharp table
        sharp_ins(st, b->x, b->y, b->f, b->g);
        sharp_ins(st, g->x, g->y, g->f, g->g);
        sharp_ins(st, w->x, w->y, w->f, w->g);

        print_rslt(*b);  print_rslt(*g);  print_rslt(*w);

    do {
        // Re-order function values
        status = reorder_vert(b, g, w);
    
        // Calculate midpoint of good side
        status = midpoint_dgetrf(b, g, &d, tol_nb, tol_nthr, st, exec, eps,
                                 m, n, A, lda, iPiv,
                                 dgetrf_time);

        // Reflection using point r
        status = reflect_dgetrf(&d, w, &r, tol_nb, tol_nthr, st, exec, eps,
                                m, n, A, lda, iPiv,
                                dgetrf_time);

        // Reflect or extend
        if (r.f < g->f) {
    
            // Replace W with R
            if (b->f < r.f)
                *w = r;
            else {
    
                // Expand using point e
                status = expand_dgetrf(&d, &r, &e, tol_nb, tol_nthr, st, exec, eps,
                                       m, n, A, lda, iPiv,
                                       dgetrf_time);

                *w = (e.f < b->f) ? e : r;
            }
        }
        // Contract or shrink
        else {
    
            if (r.f < w->f) *w = r;
    
            // Contract using point c
            status = contract_dgetrf(w, &d, &r, &c, tol_nb, tol_nthr, st, exec, eps,
                                     m, n, A, lda, iPiv,
                                     dgetrf_time);
            if (c.f < w->f)
                *w = c;
            else {
                // Shrink towards b
                status = shrink_dgetrf(b, w, &s, tol_nb, tol_nthr, st, exec, eps,
                                       m, n, A, lda, iPiv,
                                       dgetrf_time);
                *w = s;  *g = d;
            }
        }

        *iter += 1;

    } while (abs(b->x - g->x) > *tol_nb &&
             abs(b->y - g->y) > *tol_nthr);

    // Print out full sharp table
    sharp_print(st);

    // Save full sharp table into file
    sharp_save(st, fname, header);

    // Destroy sharp table
    sharp_free(st);

    return status;
}

// -----------------------------------------------------------------------------

// Calculates midpoint of good side
int midpoint_dgeqrf(vert_t *b, vert_t *g, vert_t *d, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    int m, int n, double *A, int lda, plasma_desc_t *T,
                    int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                       int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter midpoint\n");

    d->x = nm((b->x + g->x)/2, *tol_nb);
    d->y = nm((b->y + g->y)/2, *tol_nthr);

    d->f = sharp_lookup(st, d->x, d->y, 't');
    d->g = sharp_lookup(st, d->x, d->y, 'f');

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(d->f - 0.0) <= eps) {

        // @test
        // printf("Execute dgeqrf\n");

        status = (*dgeqrf_time)(m, n, A, lda, T,
                                d->x, d->y, &d->f, &d->g);  *exec += 1;

        // @test
        // printf("Store dgeqrf result\n");

        // Store data entry in sharp table
        sharp_ins(st, d->x, d->y, d->f, d->g);

        // @test
        // printf("Print dgeqrf result\n");

        print_rslt(*d);
    }

    // @test
    print_vert("midpoint", *d);

    // @test
    // printf("Exit midpoint\n");

    return status;
}


// Calculates reflection point r
int reflect_dgeqrf(vert_t *d, vert_t *w, vert_t *r, int *tol_nb, int *tol_nthr,
                   sharp_t *st, int *exec, double eps,
                   int m, int n, double *A, int lda, plasma_desc_t *T,
                   int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                      int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter reflect\n");

    r->x = nm(2*d->x - w->x, *tol_nb);
    r->y = nm(2*d->y - w->y, *tol_nthr);

    // printf("d.y: %d, w.y: %d, tol: %d\n", d->y, w->y, *tol_nthr);
    // printf("2*d.y - w.y: %d\n", 2*d->y - w->y);

    r->f = sharp_lookup(st, r->x, r->y, 't');
    r->g = sharp_lookup(st, r->x, r->y, 'f');

    // @test
    // print_vert("pre-reflection", *r);

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(r->f - 0.0) <= eps) {

        // @test
        // printf("Execute dgeqrf\n");

        status = (*dgeqrf_time)(m, n, A, lda, T,
                                r->x, r->y, &r->f, &r->g);  *exec += 1;

        // @test
        // printf("Store dgeqrf result\n");

        // Store data entry in sharp table
        sharp_ins(st, r->x, r->y, r->f, r->g);

        // @test
        // printf("Print dgeqrf result\n");

        print_rslt(*r);
    }

    // @test
    print_vert("reflection", *r);

    // @test
    // printf("Exit reflect\n");

    return status;
}


// Calculates expansion point e
int expand_dgeqrf(vert_t *r, vert_t *d, vert_t *e, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  int m, int n, double *A, int lda, plasma_desc_t *T,
                  int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                     int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter expand\n");

    e->x = nm(2*r->x - d->x, *tol_nb);
    e->y = nm(2*r->y - d->y, *tol_nthr);

    // printf("r.y: %d, d.y: %d, tol: %d\n", r->y, d->y, *tol_nthr);
    // printf("2*r.y - d.y: %d\n", 2*r->y - d->y);

    e->f = sharp_lookup(st, e->x, e->y, 't');
    e->g = sharp_lookup(st, e->x, e->y, 'f');

    // @test
    // print_vert("pre-expansion", *e);

    int status = 0;

    // Execute function, if experiment with this 'nb, nthr' value pair is not present
    if (fabs(e->f - 0.0) <= eps) {

        // @test
        // printf("Execute dgeqrf\n");

        status = (*dgeqrf_time)(m, n, A, lda, T,
                                e->x, e->y, &e->f, &e->g);  *exec += 1;

        // @test
        // printf("Store dgeqrf result\n");

        // Store data entry in sharp table
        sharp_ins(st, e->x, e->y, e->f, e->g);

        // @test
        // printf("Print dgeqrf result\n");

        print_rslt(*e);
    }

    // @test
    print_vert("expansion", *e);

    // @test
    // printf("Exit expand\n");

    return status;
}


// Calculates contraction point c
int contract_dgeqrf(vert_t *w, vert_t *d, vert_t *r, vert_t *c, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    int m, int n, double *A, int lda, plasma_desc_t *T,
                    int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                       int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter contract\n");

    vert_t c1 = (vert_t){.x = 0, .y = 0, .f = 0.0, .g = 0.0};
    vert_t c2 = (vert_t){.x = 0, .y = 0, .f = 0.0, .g = 0.0};

    int status1 = midpoint_dgeqrf(w, d, &c1, tol_nb, tol_nthr, st, exec, eps,
                                  m, n, A, lda, T,
                                  dgeqrf_time);

    int status2 = midpoint_dgeqrf(d, r, &c2, tol_nb, tol_nthr, st, exec, eps,
                                  m, n, A, lda, T,
                                  dgeqrf_time);

    *c = (c1.f < c2.f) ? c1 : c2;

    // @test
    print_vert("contraction", *c);

    // @test
    // printf("Exit contract\n");

    return status1 && status2;
}


// Calculates shrinking point s
int shrink_dgeqrf(vert_t *b, vert_t *w, vert_t *s, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  int m, int n, double *A, int lda, plasma_desc_t *T,
                  int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                     int, int, plasma_time_t*, double*)) {

    // @test
    // printf("Enter shrink\n");

    int status = midpoint_dgeqrf(b, w, s, tol_nb, tol_nthr, st, exec, eps,
                                 m, n, A, lda, T,
                                 dgeqrf_time);
    // @test
    print_vert("shrinking", *s);

    // @test
    // printf("Exit shrink\n");

    return status;
}


/*
 * Optimises Cholesky factorisation routine DPOTRF for best tile size 'nb' and
 * no. of OpenMP threads 'nthr' using Nelder-Mead algorithm
 */
int nelder_mead_dgeqrf(vert_t *b, vert_t *g, vert_t *w,
                       int *tol_nb, int *tol_nthr, int *exec, int *iter, char *fname, char *header,
                       int m, int n, double *A, int lda, plasma_desc_t *T,
                       int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                          int, int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    *exec = 0;  *iter = 0;

    // Define auxiliary vertices
    vert_t d, r, e, c, s;

    // Calculate size of sharp table (no. of function executions)
    int const K = 2*bisect_niter(b->x, w->x, *tol_nb) + 3;

    // Create sharp table to store data entries
    sharp_t *st = sharp_new(K);

    // Obtain initial triangle BGW
    int status = (*dgeqrf_time)(m, n, A, lda, T,
                                b->x, b->y, &b->f, &b->g);  *exec += 1;

        status = (*dgeqrf_time)(m, n, A, lda, T,
                                g->x, g->y, &g->f, &g->g);  *exec += 1;

        status = (*dgeqrf_time)(m, n, A, lda, T,
                                w->x, w->y, &w->f, &w->g);  *exec += 1;

        // Store data entries in sharp table
        sharp_ins(st, b->x, b->y, b->f, b->g);
        sharp_ins(st, g->x, g->y, g->f, g->g);
        sharp_ins(st, w->x, w->y, w->f, w->g);

        print_rslt(*b);  print_rslt(*g);  print_rslt(*w);

    do {
        // Re-order function values
        status = reorder_vert(b, g, w);
    
        // Calculate midpoint of good side
        status = midpoint_dgeqrf(b, g, &d, tol_nb, tol_nthr, st, exec, eps,
                                 m, n, A, lda, T,
                                 dgeqrf_time);

        // Reflection using point r
        status = reflect_dgeqrf(&d, w, &r, tol_nb, tol_nthr, st, exec, eps,
                                m, n, A, lda, T,
                                dgeqrf_time);

        // Reflect or extend
        if (r.f < g->f) {
    
            // Replace W with R
            if (b->f < r.f)
                *w = r;
            else {
    
                // Expand using point e
                status = expand_dgeqrf(&d, &r, &e, tol_nb, tol_nthr, st, exec, eps,
                                       m, n, A, lda, T,
                                       dgeqrf_time);

                *w = (e.f < b->f) ? e : r;
            }
        }
        // Contract or shrink
        else {
    
            if (r.f < w->f) *w = r;
    
            // Contract using point c
            status = contract_dgeqrf(w, &d, &r, &c, tol_nb, tol_nthr, st, exec, eps,
                                     m, n, A, lda, T,
                                     dgeqrf_time);
            if (c.f < w->f)
                *w = c;
            else {
                // Shrink towards b
                status = shrink_dgeqrf(b, w, &s, tol_nb, tol_nthr, st, exec, eps,
                                       m, n, A, lda, T,
                                       dgeqrf_time);
                *w = s;  *g = d;
            }
        }

        *iter += 1;

    } while (abs(b->x - g->x) > *tol_nb &&
             abs(b->y - g->y) > *tol_nthr);

    // Print out full sharp table
    sharp_print(st);

    // Save full sharp table into file
    sharp_save(st, fname, header);

    // Destroy sharp table
    sharp_free(st);

    return status;
}

// EoF: nelder_mead.c
