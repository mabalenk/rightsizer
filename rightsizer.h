/*
 * @brief Prototype rightsizer implementation.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jun 19, 2017
 * @version 0.12
 */

int rightsize_dgemm(int *range_nb, int tol_nb, int *range_nthr, int tol_nthr,
                    int m, int n, int k, double alpha, double beta);

int rightsize_dgeqrf(int *range_nb, int tol_nb, int *range_nthr, int tol_nthr, int ib, char hmode,
                     int m, int n);

int rightsize_dgetrf(int *range_nb, int tol_nb, int *range_nthr, int tol_nthr, int ib, int mtpf,
                     int m, int n);

int rightsize_dpotrf(int *range_nb, int tol_nb, int *range_nthr, int tol_nthr,
                     int n);

// EoF: rightsizer.h
