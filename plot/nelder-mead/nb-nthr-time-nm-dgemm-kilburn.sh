set terminal postscript eps enhanced colour solid font "Helvetica, 22";
# set terminal png enhanced;

set dgrid3d 20, 20;
set hidden3d;
set lmargin 4;
set key top left Left;
set format "%g";
set output "nb-nthr-time-nm-dgemm-kilburn.eps";
set title  "DGEMM, Nelder-Mead (nb, nthr) \\@ Kilburn";
set xlabel "nb";
set ylabel "n_{thr}";
set zlabel "t (s)";
set grid;
set pm3d;
# set logscale x;

splot "nelder.mead.dgemm" u ($1):($2):($3) t "" w l lw 5;
