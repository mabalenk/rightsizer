/*
 * @brief Sharp table tester.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 22, 2017
 * @version 0.1
 */

#include "sharp_table.h"

#include <stdio.h>


int main(int argc, char *argv[]) {

    printf("Testing sharp table functionality...\n");

    sharp_t *s = sharp_new(10);

    // Test insertion functionality
    sharp_ins(s,  64,  8, 30.123, 123.456);
    sharp_ins(s,  16, 16, 25.098, 213.456);
    sharp_ins(s,  32, 10, 29.678, 312.456);
    sharp_ins(s,   8,  2, 15.024, 456.456);
    sharp_ins(s,   4,  1, 10.241, 546.456);
    sharp_ins(s, 128, 20, 95.395, 645.456);
    sharp_ins(s,   2,  4, 87.032, 762.456);
    sharp_ins(s, 256, 24, 46.789, 267.456);
    sharp_ins(s, 512, 12, 31.098, 672.456);
    sharp_ins(s,  96,  8, 28.543, 627.456);
    sharp_print(s);

    // Test re-hashing functionality
    sharp_ins(s, 512, 16, 12.753, 531.097);

    // Test duplicate insertion functionality
    sharp_ins(s, 512, 16, 12.890, 529.097);
    sharp_ins(s, 128, 20, 93.890, 647.097);
    sharp_print(s);

    sharp_free(s);

    printf("Sharp table test has been completed.\n");

    return 0;
}

// EoF: test_sharp_table.c
