/*
 * @brief Bisection and golden section implementation.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jun 23, 2017
 * @version 0.13
 */

#include "hash_table.h"
#include "plasma.h"
#include "test.h"


int nm(int n, int m);

double fmin_arr(double *arr, int len);

int bisect_niter(int a, int b, int x);

double sign(double a, double b);


void bracket_min_dgemm(int *xa, int *xb, int *xc, int *tol, hash_t *h, int *exec,
                       plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                       double alpha, double *A, int lda, double *B, int ldb,  double beta, double *C, int ldc,
                       int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                         double*, int, double*, int, double, double*, int,
                                         int, plasma_time_t*, double*));

void bracket_min_dgeqrf(int *xa, int *xb, int *xc, int *tol, hash_t *h, int *exec,
                        int m, int n, double *A, int lda, plasma_desc_t *T,
                        int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                           int, plasma_time_t*, double*));

void bracket_min_dgetrf(int *xa, int *xb, int *xc, int *tol, hash_t *h, int *exec,
                        int m, int n, double *A, int lda, int *iPiv,
                        int (*dgetrf_time)(int, int, double*, int, int*,
                                           int, plasma_time_t*, double*));

void bracket_min_dpotrf(int *xa, int *xb, int *xc, int *tol, hash_t *h, int *exec,
                        plasma_enum_t uplo, int n, double *A, int lda,
                        int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                           int, plasma_time_t*, double*));


int bisect_dgemm(int a, int b, int *tol, int *nb, int *exec, int *iter, char *fname, char *header,
                 plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                 double alpha, double *A, int lda, double *B, int ldb, double beta, double *C, int ldc,
                 int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                   double*, int, double*, int, double, double*, int,
                                   int, plasma_time_t*, double*));

int bisect_dgeqrf(int a, int b, int *tol, int *nb, int *exec, int *iter, char *fname, char *header,
                  int m, int n, double *A, int lda, plasma_desc_t *T,
                  int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                     int, plasma_time_t*, double*));

int bisect_dgetrf(int a, int b, int *tol, int *nb, int *exec, int *iter, char *fname, char *header,
                  int m, int n, double *A, int lda, int *iPiv,
                  int (*dgetrf_time)(int, int, double*, int, int*,
                                     int, plasma_time_t*, double*));

int bisect_dpotrf(int a, int b, int *tol, int *nb, int *exec, int *iter, char *fname, char *header,
                  plasma_enum_t uplo, int n, double *A, int lda,
                  int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                     int, plasma_time_t*, double*));


int goldsect_dgemm(int a, int b, int c, int *tol, int *nb, int *exec, int *iter,
                   char *fname, char *header,
                   plasma_enum_t transA, plasma_enum_t transB,
                   int m, int n, int k, double alpha, double *A, int lda, double *B,
                   int ldb, double beta, double *C, int ldc,
                   int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                     double*, int, double*, int, double, double*, int,
                                     int, plasma_time_t*, double*));

int goldsect_dgeqrf(int a, int b, int c, int *tol, int *nb, int *exec, int *iter,
                    char *fname, char *header,
                    int m, int n, double *A, int lda, plasma_desc_t *T,
                    int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                       int, plasma_time_t*, double*));

int goldsect_dgetrf(int a, int b, int c, int *tol, int *nb, int *exec, int *iter,
                    char *fname, char *header,
                    int m, int n, double *A, int lda, int *iPiv,
                    int (*dgetrf_time)(int, int, double*, int, int*,
                                       int, plasma_time_t*, double*));

int goldsect_dpotrf(int a, int b, int c, int *tol, int *nb, int *exec, int *iter,
                    char *fname, char *header,
                    plasma_enum_t uplo, int n, double *A, int lda,
                    int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                       int, plasma_time_t*, double*));
// EoF: bisect.h
