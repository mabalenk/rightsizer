/*
 * @brief Prototype rightsizer implementation.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 24, 2017
 * @version 0.15
 */

#include "test.h"
#include "flops.h"
#include "core_lapack.h"
#include "plasma.h"
#include "plasma_tuning.h"
#include "rightsizer.h"

#include <omp.h>


// Test function for Nelder-Mead algorithm
int function(double x, double y, double *f) {

    *f = x*x - 4.0*x + y*y - y - x*y;

    return 0;
}


// Performs matrix-matrix multiply, measures time and GFLOP/s
int dgemm_time_nb(plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                  double alpha, double *pA, int lda, double *pB, int ldb, double beta,
                  double *pC, int ldc, int nb, plasma_time_t *time, double *flops) {

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Multiply matrices C:= alpha*[A]*[B] + beta*C
    plasma_time_t start = omp_get_wtime();

    int info = plasma_dgemm(transA, transB, m, n, k, alpha, pA, lda, pB, ldb, beta, pC, ldc);

    plasma_time_t stop  = omp_get_wtime();

    *time  = stop-start;
    *flops = flops_dgemm(m, n, k) / *time / 1e9;

    return info;
}


// Performs QR factorisation of matrix, measures time and GFLOP/s
int dgeqrf_time_nb(int m, int n, double *pA, int lda, plasma_desc_t *T,
                   int nb, plasma_time_t *time, double *flops) {

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Factorise matrix A = Q*R
    plasma_time_t start = omp_get_wtime();

    int info = plasma_dgeqrf(m, n, pA, lda, T);

    plasma_time_t stop  = omp_get_wtime();

    *time  = stop-start;
    *flops = flops_dgeqrf(m, n) / *time / 1e9;

    return info;
}


// Performs LU factorisation of matrix, measures time and GFLOP/s
int dgetrf_time_nb(int m, int n, double *pA, int lda, int *iPiv,
                   int nb, plasma_time_t *time, double *flops) {

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Factorise matrix A = L*U
    plasma_time_t start = omp_get_wtime();

    int info = plasma_dgetrf(m, n, pA, lda, iPiv);

    plasma_time_t stop  = omp_get_wtime();

    *time  = stop-start;
    *flops = flops_dgetrf(m, n) / *time / 1e9;

    return info;
}


/*
 * Performs Cholesky factorisation of symmetric positive definite matrix,
 * measures time and GFLOP/s
 */
int dpotrf_time_nb(plasma_enum_t uplo, int n, double *pA, int lda,
                   int nb, plasma_time_t *time, double *flops) {

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Factorise matrix A = L*L^T or A = U^T*U using Cholesky algorithm
    plasma_time_t start = omp_get_wtime();

    int info = plasma_dpotrf(uplo, n, pA, lda);

    plasma_time_t stop  = omp_get_wtime();

    *time  = stop-start;
    *flops = flops_dpotrf(n) / *time / 1e9;

    return info;
}


// Performs matrix-matrix multiply, measures time and GFLOP/s
int dgemm_time_nthr(plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                    double alpha, double *pA, int lda, double *pB, int ldb, double beta,
                    double *pC, int ldc, int nthr, plasma_time_t *time, double *flops) {

    // Multiply matrices C:= alpha*[A]*[B] + beta*C
    plasma_time_t start = omp_get_wtime();

    // Get PLASMA context.
    plasma_context_t *plasma = plasma_context_self();
    if (plasma == NULL) {
        plasma_error("PLASMA not initialized");
        return PlasmaErrorNotInitialized;
    }

    // Check input arguments.
    if ((transA != PlasmaNoTrans) &&
        (transA != PlasmaTrans) &&
        (transA != PlasmaConjTrans)) {
        plasma_error("illegal value of transA");
        return -1;
    }
    if ((transB != PlasmaNoTrans) &&
        (transB != PlasmaTrans) &&
        (transB != PlasmaConjTrans)) {
        plasma_error("illegal value of transB");
        return -2;
    }
    if (m < 0) {
        plasma_error("illegal value of m");
        return -3;
    }
    if (n < 0) {
        plasma_error("illegal value of n");
        return -4;
    }
    if (k < 0) {
        plasma_error("illegal value of k");
        return -5;
    }

    int am, an;
    int bm, bn;
    if (transA == PlasmaNoTrans) {
        am = m;
        an = k;
    }
    else {
        am = k;
        an = m;
    }
    if (transB == PlasmaNoTrans) {
        bm = k;
        bn = n;
    }
    else {
        bm = n;
        bn = k;
    }

    if (lda < imax(1, am)) {
        plasma_error("illegal value of lda");
        return -8;
    }
    if (ldb < imax(1, bm)) {
        plasma_error("illegal value of ldb");
        return -10;
    }
    if (ldc < imax(1, m)) {
        plasma_error("illegal value of ldc");
        return -13;
    }

    // quick return
    if (m == 0 || n == 0 || ((alpha == 0.0 || k == 0) && beta == 1.0))
        return PlasmaSuccess;

    // Tune parameters.
    if (plasma->tuning)
        plasma_tune_gemm(plasma, PlasmaComplexDouble, m, n, k);

    // Set tiling parameters.
    int nb = plasma->nb;

    // Create tile matrices.
    plasma_desc_t A;
    plasma_desc_t B;
    plasma_desc_t C;
    int retval;
    retval = plasma_desc_general_create(PlasmaComplexDouble, nb, nb,
                                        am, an, 0, 0, am, an, &A);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        return retval;
    }
    retval = plasma_desc_general_create(PlasmaComplexDouble, nb, nb,
                                        bm, bn, 0, 0, bm, bn, &B);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        plasma_desc_destroy(&A);
        return retval;
    }
    retval = plasma_desc_general_create(PlasmaComplexDouble, nb, nb,
                                        m, n, 0, 0, m, n, &C);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        plasma_desc_destroy(&A);
        plasma_desc_destroy(&B);
        return retval;
    }

    // Initialise sequence.
    plasma_sequence_t sequence;
    retval = plasma_sequence_init(&sequence);

    // Initialize request.
    plasma_request_t request;
    retval = plasma_request_init(&request);

    // asynchronous block
    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Translate to tile layout.
        plasma_omp_dge2desc(pA, lda, A, &sequence, &request);
        plasma_omp_dge2desc(pB, ldb, B, &sequence, &request);
        plasma_omp_dge2desc(pC, ldc, C, &sequence, &request);

        // Call the tile async function.
        plasma_omp_dgemm(transA, transB,
                         alpha, A,
                                B,
                         beta,  C,
                         &sequence, &request);

        // Translate back to LAPACK layout.
        plasma_omp_ddesc2ge(C, pC, ldc, &sequence, &request);
    }
    // implicit synchronization

    // Free matrices in tile layout.
    plasma_desc_destroy(&A);
    plasma_desc_destroy(&B);
    plasma_desc_destroy(&C);

    plasma_time_t stop  = omp_get_wtime();
    
    *time  = stop-start;
    *flops = flops_dgemm(m, n, k) / *time / 1e9;

    // Return status.
    return sequence.status;
}


// Performs QR factorisation of matrix, measures time and GFLOP/s
int dgeqrf_time_nthr(int m, int n, double *pA, int lda, plasma_desc_t *T,
                     int nthr, plasma_time_t *time, double *flops) {

    // Factorise matrix A = Q*R
    plasma_time_t start = omp_get_wtime();

    // Get PLASMA context.
    plasma_context_t *plasma = plasma_context_self();
    if (plasma == NULL) {
        plasma_fatal_error("PLASMA not initialized");
        return PlasmaErrorNotInitialized;
    }

    // Check input arguments.
    if (m < 0) {
        plasma_error("illegal value of m");
        return -1;
    }
    if (n < 0) {
        plasma_error("illegal value of n");
        return -2;
    }
    if (lda < imax(1, m)) {
        plasma_error("illegal value of lda");
        return -4;
    }

    // quick return
    if (imin(m, n) == 0)
        return PlasmaSuccess;

    // Tune parameters.
    if (plasma->tuning)
        plasma_tune_geqrf(plasma, PlasmaRealDouble, m, n);

    // Set tiling parameters.
    int ib = plasma->ib;
    int nb = plasma->nb;
    int householder_mode = plasma->householder_mode;

    // Create tile matrix.
    plasma_desc_t A;
    int retval;
    retval = plasma_desc_general_create(PlasmaRealDouble, nb, nb,
                                        m, n, 0, 0, m, n, &A);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        return retval;
    }

    // Prepare descriptor T.
    retval = plasma_descT_create(A, ib, householder_mode, T);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_descT_create() failed");
        return retval;
    }

    // Allocate workspace.
    plasma_workspace_t work;
    size_t lwork = nb + ib*nb;  // geqrt: tau + work
    retval = plasma_workspace_create(&work, lwork, PlasmaRealDouble);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_workspace_create() failed");
        return retval;
    }

    // Initialize sequence.
    plasma_sequence_t sequence;
    retval = plasma_sequence_init(&sequence);

    // Initialize request.
    plasma_request_t request;
    retval = plasma_request_init(&request);

    // asynchronous block
    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Translate to tile layout.
        plasma_omp_dge2desc(pA, lda, A, &sequence, &request);

        // Call the tile async function.
        plasma_omp_dgeqrf(A, *T, work, &sequence, &request);

        // Translate back to LAPACK layout.
        plasma_omp_ddesc2ge(A, pA, lda, &sequence, &request);
    }
    // implicit synchronization

    plasma_workspace_destroy(&work);

    // Free matrix A in tile layout.
    plasma_desc_destroy(&A);

    plasma_time_t stop  = omp_get_wtime();

    *time  = stop-start;
    *flops = flops_dgeqrf(m, n) / *time / 1e9;

    // Return status.
    return sequence.status;
}


// Performs LU factorisation of matrix, measures time and GFLOP/s
int dgetrf_time_nthr(int m, int n, double *pA, int lda, int *iPiv,
                     int nthr, plasma_time_t *time, double *flops) {

    // Factorise matrix A = LU using LU algorithm
    plasma_time_t start = omp_get_wtime();

    // Get PLASMA context.
    plasma_context_t *plasma = plasma_context_self();
    if (plasma == NULL) {
        plasma_fatal_error("PLASMA not initialized");
        return PlasmaErrorNotInitialized;
    }

    if (m < 0) {
        plasma_error("illegal value of m");
        return -1;
    }
    if (n < 0) {
        plasma_error("illegal value of n");
        return -2;
    }
    if (lda < imax(1, m)) {
        plasma_error("illegal value of lda");
        return -4;
    }

    // quick return
    if (imin(m, n) == 0)
        return PlasmaSuccess;

    // Tune parameters.
    if (plasma->tuning)
        plasma_tune_getrf(plasma, PlasmaRealDouble, m, n);

    // Set tiling parameters.
    int nb = plasma->nb;

    // Initialize barrier.
    plasma_barrier_init(&plasma->barrier);

    // Create tile matrix.
    plasma_desc_t A;
    int retval;
    retval = plasma_desc_general_create(PlasmaRealDouble, nb, nb,
                                        m, n, 0, 0, m, n, &A);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        return retval;
    }

    // Initialize sequence.
    plasma_sequence_t sequence;
    retval = plasma_sequence_init(&sequence);

    // Initialize request.
    plasma_request_t request;
    retval = plasma_request_init(&request);

    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Translate to tile layout.
        plasma_omp_dge2desc(pA, lda, A, &sequence, &request);
    }

    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Call the tile async function.
        plasma_omp_dgetrf(A, iPiv, &sequence, &request);
    }

    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Translate back to LAPACK layout.
        plasma_omp_ddesc2ge(A, pA, lda, &sequence, &request);
    }

    // Free matrix A in tile layout.
    plasma_desc_destroy(&A);

    plasma_time_t stop  = omp_get_wtime();
    
    *time  = stop-start;
    *flops = flops_dgetrf(m, n) / *time / 1e9;

    // Return status.
    return sequence.status;
}


/*
 * Performs Cholesky factorisation of symmetric positive definite matrix,
 * measures time and GFLOP/s
 */
int dpotrf_time_nthr(plasma_enum_t uplo, int n, double *pA, int lda,
                     int nthr, plasma_time_t *time, double *flops) {

    // Factorise matrix A = L*L^T or A = U^T*U using Cholesky algorithm
    plasma_time_t start = omp_get_wtime();

    // Get PLASMA context.
    plasma_context_t *plasma = plasma_context_self();
    if (plasma == NULL) {
        plasma_fatal_error("PLASMA not initialized");
        return PlasmaErrorNotInitialized;
    }

    // Check input arguments.
    if ((uplo != PlasmaUpper) &&
        (uplo != PlasmaLower)) {
        plasma_error("illegal value of uplo");
        return -1;
    }
    if (n < 0) {
        plasma_error("illegal value of n");
        return -2;
    }
    if (lda < imax(1, n)) {
        plasma_error("illegal value of lda");
        return -4;
    }

    // quick return
    if (imax(n, 0) == 0)
        return PlasmaSuccess;

    // Tune parameters.
    if (plasma->tuning)
        plasma_tune_potrf(plasma, PlasmaRealDouble, n);

    // Set tiling parameters.
    int nb = plasma->nb;

    // Create tile matrix.
    plasma_desc_t A;
    int retval;
    retval = plasma_desc_triangular_create(PlasmaRealDouble, uplo, nb, nb,
                                           n, n, 0, 0, n, n, &A);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        return retval;
    }

    // Initialize sequence.
    plasma_sequence_t sequence;
    retval = plasma_sequence_init(&sequence);

    // Initialize request.
    plasma_request_t request;
    retval = plasma_request_init(&request);

    // asynchronous block
    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Translate to tile layout.
        plasma_omp_dtr2desc(pA, lda, A, &sequence, &request);

        // Call the tile async function.
        plasma_omp_dpotrf(uplo, A, &sequence, &request);

        // Translate back to LAPACK layout.
        plasma_omp_ddesc2tr(A, pA, lda, &sequence, &request);
    }
    // implicit synchronization

    // Free matrix A in tile layout.
    plasma_desc_destroy(&A);

    plasma_time_t stop = omp_get_wtime();

    *time  = stop-start;
    *flops = flops_dpotrf(n) / *time / 1e9;

    // Return status.
    return sequence.status;
}


// Performs matrix-matrix multiply, measures time and GFLOP/s
int dgemm_time(plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
               double alpha, double *pA, int lda, double *pB, int ldb, double beta,
               double *pC, int ldc, int nb, int nthr, plasma_time_t *time, double *flops) {

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Multiply matrices C:= alpha*[A]*[B] + beta*C
    plasma_time_t start = omp_get_wtime();

    // Get PLASMA context.
    plasma_context_t *plasma = plasma_context_self();
    if (plasma == NULL) {
        plasma_error("PLASMA not initialized");
        return PlasmaErrorNotInitialized;
    }

    // Check input arguments.
    if ((transA != PlasmaNoTrans) &&
        (transA != PlasmaTrans) &&
        (transA != PlasmaConjTrans)) {
        plasma_error("illegal value of transA");
        return -1;
    }
    if ((transB != PlasmaNoTrans) &&
        (transB != PlasmaTrans) &&
        (transB != PlasmaConjTrans)) {
        plasma_error("illegal value of transB");
        return -2;
    }
    if (m < 0) {
        plasma_error("illegal value of m");
        return -3;
    }
    if (n < 0) {
        plasma_error("illegal value of n");
        return -4;
    }
    if (k < 0) {
        plasma_error("illegal value of k");
        return -5;
    }

    int am, an;
    int bm, bn;
    if (transA == PlasmaNoTrans) {
        am = m;
        an = k;
    }
    else {
        am = k;
        an = m;
    }
    if (transB == PlasmaNoTrans) {
        bm = k;
        bn = n;
    }
    else {
        bm = n;
        bn = k;
    }

    if (lda < imax(1, am)) {
        plasma_error("illegal value of lda");
        return -8;
    }
    if (ldb < imax(1, bm)) {
        plasma_error("illegal value of ldb");
        return -10;
    }
    if (ldc < imax(1, m)) {
        plasma_error("illegal value of ldc");
        return -13;
    }

    // quick return
    if (m == 0 || n == 0 || ((alpha == 0.0 || k == 0) && beta == 1.0))
        return PlasmaSuccess;

    // Tune parameters.
    if (plasma->tuning)
        plasma_tune_gemm(plasma, PlasmaComplexDouble, m, n, k);

    // Create tile matrices.
    plasma_desc_t A;
    plasma_desc_t B;
    plasma_desc_t C;
    int retval;
    retval = plasma_desc_general_create(PlasmaComplexDouble, nb, nb,
                                        am, an, 0, 0, am, an, &A);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        return retval;
    }
    retval = plasma_desc_general_create(PlasmaComplexDouble, nb, nb,
                                        bm, bn, 0, 0, bm, bn, &B);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        plasma_desc_destroy(&A);
        return retval;
    }
    retval = plasma_desc_general_create(PlasmaComplexDouble, nb, nb,
                                        m, n, 0, 0, m, n, &C);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        plasma_desc_destroy(&A);
        plasma_desc_destroy(&B);
        return retval;
    }

    // Initialise sequence.
    plasma_sequence_t sequence;
    retval = plasma_sequence_init(&sequence);

    // Initialize request.
    plasma_request_t request;
    retval = plasma_request_init(&request);

    // asynchronous block
    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Translate to tile layout.
        plasma_omp_dge2desc(pA, lda, A, &sequence, &request);
        plasma_omp_dge2desc(pB, ldb, B, &sequence, &request);
        plasma_omp_dge2desc(pC, ldc, C, &sequence, &request);

        // Call the tile async function.
        plasma_omp_dgemm(transA, transB,
                         alpha, A,
                                B,
                         beta,  C,
                         &sequence, &request);

        // Translate back to LAPACK layout.
        plasma_omp_ddesc2ge(C, pC, ldc, &sequence, &request);
    }
    // implicit synchronization

    // Free matrices in tile layout.
    plasma_desc_destroy(&A);
    plasma_desc_destroy(&B);
    plasma_desc_destroy(&C);

    plasma_time_t stop  = omp_get_wtime();
    
    *time  = stop-start;
    *flops = flops_dgemm(m, n, k) / *time / 1e9;

    // Return status.
    return sequence.status;
}


// Performs QR factorisation of matrix, measures time and GFLOP/s
int dgeqrf_time(int m, int n, double *pA, int lda, plasma_desc_t *T,
                int nb, int nthr, plasma_time_t *time, double *flops) {

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Factorise matrix A = Q*R
    plasma_time_t start = omp_get_wtime();

    // Get PLASMA context.
    plasma_context_t *plasma = plasma_context_self();
    if (plasma == NULL) {
        plasma_fatal_error("PLASMA not initialized");
        return PlasmaErrorNotInitialized;
    }

    // Check input arguments.
    if (m < 0) {
        plasma_error("illegal value of m");
        return -1;
    }
    if (n < 0) {
        plasma_error("illegal value of n");
        return -2;
    }
    if (lda < imax(1, m)) {
        plasma_error("illegal value of lda");
        return -4;
    }

    // quick return
    if (imin(m, n) == 0)
        return PlasmaSuccess;

    // Tune parameters.
    if (plasma->tuning)
        plasma_tune_geqrf(plasma, PlasmaRealDouble, m, n);

    // Set tiling parameters.
    int ib = plasma->ib;
    int householder_mode = plasma->householder_mode;

    // Create tile matrix.
    plasma_desc_t A;
    int retval;
    retval = plasma_desc_general_create(PlasmaRealDouble, nb, nb,
                                        m, n, 0, 0, m, n, &A);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        return retval;
    }

    // Prepare descriptor T.
    retval = plasma_descT_create(A, ib, householder_mode, T);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_descT_create() failed");
        return retval;
    }

    // Allocate workspace.
    plasma_workspace_t work;
    size_t lwork = nb + ib*nb;  // geqrt: tau + work
    retval = plasma_workspace_create(&work, lwork, PlasmaRealDouble);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_workspace_create() failed");
        return retval;
    }

    // Initialize sequence.
    plasma_sequence_t sequence;
    retval = plasma_sequence_init(&sequence);

    // Initialize request.
    plasma_request_t request;
    retval = plasma_request_init(&request);

    // asynchronous block
    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Translate to tile layout.
        plasma_omp_dge2desc(pA, lda, A, &sequence, &request);

        // Call the tile async function.
        plasma_omp_dgeqrf(A, *T, work, &sequence, &request);

        // Translate back to LAPACK layout.
        plasma_omp_ddesc2ge(A, pA, lda, &sequence, &request);
    }
    // implicit synchronization

    plasma_workspace_destroy(&work);

    // Free matrix A in tile layout.
    plasma_desc_destroy(&A);

    plasma_time_t stop  = omp_get_wtime();

    *time  = stop-start;
    *flops = flops_dgeqrf(m, n) / *time / 1e9;

    // Return status.
    return sequence.status;
}


/*
 * Performs Cholesky factorisation of symmetric positive definite matrix,
 * measures time and GFLOP/s
 */
int dpotrf_time(plasma_enum_t uplo, int n, double *pA, int lda,
                int nb, int nthr, plasma_time_t *time, double *flops) {

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Factorise matrix A = L*L^T or A = U^T*U using Cholesky algorithm
    plasma_time_t start = omp_get_wtime();

    // Get PLASMA context.
    plasma_context_t *plasma = plasma_context_self();
    if (plasma == NULL) {
        plasma_fatal_error("PLASMA not initialized");
        return PlasmaErrorNotInitialized;
    }

    // Check input arguments.
    if ((uplo != PlasmaUpper) &&
        (uplo != PlasmaLower)) {
        plasma_error("illegal value of uplo");
        return -1;
    }
    if (n < 0) {
        plasma_error("illegal value of n");
        return -2;
    }
    if (lda < imax(1, n)) {
        plasma_error("illegal value of lda");
        return -4;
    }

    // quick return
    if (imax(n, 0) == 0)
        return PlasmaSuccess;

    // Tune parameters.
    if (plasma->tuning)
        plasma_tune_potrf(plasma, PlasmaRealDouble, n);

    // Create tile matrix.
    plasma_desc_t A;
    int retval;
    retval = plasma_desc_triangular_create(PlasmaRealDouble, uplo, nb, nb,
                                           n, n, 0, 0, n, n, &A);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        return retval;
    }

    // Initialize sequence.
    plasma_sequence_t sequence;
    retval = plasma_sequence_init(&sequence);

    // Initialize request.
    plasma_request_t request;
    retval = plasma_request_init(&request);

    // asynchronous block
    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Translate to tile layout.
        plasma_omp_dtr2desc(pA, lda, A, &sequence, &request);

        // Call the tile async function.
        plasma_omp_dpotrf(uplo, A, &sequence, &request);

        // Translate back to LAPACK layout.
        plasma_omp_ddesc2tr(A, pA, lda, &sequence, &request);
    }
    // implicit synchronization

    // Free matrix A in tile layout.
    plasma_desc_destroy(&A);

    plasma_time_t stop = omp_get_wtime();

    *time  = stop-start;
    *flops = flops_dpotrf(n) / *time / 1e9;

    // Return status.
    return sequence.status;
}


// Performs LU factorisation of matrix, measures time and GFLOP/s
int dgetrf_time(int m, int n, double *pA, int lda, int *iPiv,
                int nb, int nthr, plasma_time_t *time, double *flops) {

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Factorise matrix A = LU using LU algorithm
    plasma_time_t start = omp_get_wtime();

    // Get PLASMA context.
    plasma_context_t *plasma = plasma_context_self();
    if (plasma == NULL) {
        plasma_fatal_error("PLASMA not initialized");
        return PlasmaErrorNotInitialized;
    }

    if (m < 0) {
        plasma_error("illegal value of m");
        return -1;
    }
    if (n < 0) {
        plasma_error("illegal value of n");
        return -2;
    }
    if (lda < imax(1, m)) {
        plasma_error("illegal value of lda");
        return -4;
    }

    // quick return
    if (imin(m, n) == 0)
        return PlasmaSuccess;

    // Tune parameters.
    if (plasma->tuning)
        plasma_tune_getrf(plasma, PlasmaRealDouble, m, n);

    // Initialize barrier.
    plasma_barrier_init(&plasma->barrier);

    // Create tile matrix.
    plasma_desc_t A;
    int retval;
    retval = plasma_desc_general_create(PlasmaRealDouble, nb, nb,
                                        m, n, 0, 0, m, n, &A);
    if (retval != PlasmaSuccess) {
        plasma_error("plasma_desc_general_create() failed");
        return retval;
    }

    // Initialize sequence.
    plasma_sequence_t sequence;
    retval = plasma_sequence_init(&sequence);

    // Initialize request.
    plasma_request_t request;
    retval = plasma_request_init(&request);

    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Translate to tile layout.
        plasma_omp_dge2desc(pA, lda, A, &sequence, &request);
    }

    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Call the tile async function.
        plasma_omp_dgetrf(A, iPiv, &sequence, &request);
    }

    #pragma omp parallel num_threads(nthr)
    #pragma omp master
    {
        // Translate back to LAPACK layout.
        plasma_omp_ddesc2ge(A, pA, lda, &sequence, &request);
    }

    // Free matrix A in tile layout.
    plasma_desc_destroy(&A);

    plasma_time_t stop  = omp_get_wtime();
    
    *time  = stop-start;
    *flops = flops_dgetrf(m, n) / *time / 1e9;

    // Return status.
    return sequence.status;
}

// EoF: func.c
