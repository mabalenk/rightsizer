/*
 * @brief Prototype rightsizer implementation.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 25, 2017
 * @version 0.18
 */

#include "test.h"
#include "flops.h"
#include "core_lapack.h"
#include "plasma.h"
#include "plasma_tuning.h"

#include "bisect.h"
#include "func.h"
#include "nelder_mead.h"

#include <omp.h>

#define A(i_, j_) A[(i_) + (size_t)lda*(j_)]


// Rightsizes matrix multiplication routine 'DGEMM' for optimal tile size 'nb' and no. of OpenMP threads 'nthr'
int rightsize_dgemm(int *range_nb, int tol_nb, int *range_nthr, int tol_nthr,
                    int m, int n, int k, double alpha, double beta) {

    // Rightsizer parameters
    int nb   = 0;
    int nthr = 0;
    int exec = 0;
    int iter = 0;

    // PLASMA parameters
    plasma_enum_t transA = PlasmaNoTrans;
    plasma_enum_t transB = PlasmaNoTrans;

    int lda, ldb, ldc;
    int  an,  bn,  cn;

    if (transA == PlasmaNoTrans) {
        lda = imax(1, m);  an = k;
    }
    else {
        lda = imax(1, k);  an = m;
    }

    if (transB == PlasmaNoTrans) {
        ldb = imax(1, k);  bn = n;
    }
    else {
        ldb = imax(1, n);  bn = k;
    }

    ldc = imax(1, m);  cn = n;

    // Allocate memory
    double *A = (double*)malloc((size_t)lda*an*sizeof(double));
    assert(A != NULL);

    double *B = (double*)malloc((size_t)ldb*bn*sizeof(double));
    assert(B != NULL);

    double *C = (double*)malloc((size_t)ldc*cn*sizeof(double));
    assert(C != NULL);

    int seed[] = {0, 0, 0, 1};

    // Initialize matrices
    lapack_int retval = LAPACKE_dlarnv(1, seed, (size_t)lda*an, A);
    assert(retval == 0);

    retval = LAPACKE_dlarnv(1, seed, (size_t)ldb*bn, B);
    assert(retval == 0);

    retval = LAPACKE_dlarnv(1, seed, (size_t)ldc*cn, C);
    assert(retval == 0);

    printf("Rightsizing DGEMM...\n");

    // Initialize PLASMA
    plasma_init();

    // Set tuning parameters
    plasma_set(PlasmaTuning, PlasmaDisabled);

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);


    //--------------------------------------------------------------------------
    printf("Detecting optimal tile size 'nb' with bisection...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  time  |  GFLOP/s\n");

    bisect_dgemm(range_nb[0], range_nb[2], &tol_nb, &nb, &exec, &iter,
                "dgemm.bisect.nb", "nb  |  time  |  GFLOP/s",
                 transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                 dgemm_time_nb);

    printf("nb(exec|iter): %d(%d|%d)\n", nb, exec, iter);

    // @test
    // nb = 704;

    //--------------------------------------------------------------------------
    printf("\nDetecting optimal no. of threads 'nthr' with bisection...");
    printf("\nUsing tile size 'nb=%d'...\n", nb);
    //--------------------------------------------------------------------------
    printf("nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);

    bisect_dgemm(range_nthr[0], range_nthr[2], &tol_nthr, &nthr, &exec, &iter,
                "dgemm.bisect.nthr", "nthr  |  time  |  GFLOP/s",
                 transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                 dgemm_time_nthr);

    printf("nthr(exec|iter): %d(%d|%d)\n", nthr, exec, iter);


    //--------------------------------------------------------------------------
    printf("\nDetecting optimal tile size 'nb' with golden section...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    goldsect_dgemm(range_nb[0], range_nb[1], range_nb[2], &tol_nb, &nb, &exec, &iter,
                  "dgemm.goldsect.nb", "nb  |  time  |  GFLOP/s",
                   transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                   dgemm_time_nb);

    printf("nb(exec|iter): %d(%d|%d)\n", nb, exec, iter);

    // @test
    // nb = 232;

    //--------------------------------------------------------------------------
    printf("\nDetecting optimal no. of threads 'nthr' with golden section...");
    printf("\nUsing tile size 'nb=%d'...\n", nb);
    //--------------------------------------------------------------------------
    printf("nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);

    goldsect_dgemm(range_nthr[0], range_nthr[1], range_nthr[2], &tol_nthr, &nthr, &exec, &iter,
                  "dgemm.goldsect.nthr", "nthr  |  time  |  GFLOP/s",
                   transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                   dgemm_time_nthr);

    printf("nthr(exec|iter): %d(%d|%d)\n", nthr, exec, iter);


    /*
    //--------------------------------------------------------------------------
    printf("\nDetecting optimal tile size 'nb' and no. of threads 'nthr' "
           "with Nelder-Mead algorithm...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Define initial vertices
    vert_t z1 = (vert_t){.x = range_nb[0], .y = range_nthr[0], .f = 0.0, .g = 0.0};
    vert_t z2 = (vert_t){.x = range_nb[1], .y = range_nthr[1], .f = 0.0, .g = 0.0};
    vert_t z3 = (vert_t){.x = range_nb[2], .y = range_nthr[2], .f = 0.0, .g = 0.0};

    nelder_mead_dgemm(&z1, &z2, &z3, &tol_nb, &tol_nthr, &exec, &iter,
                     "dgemm.nelder.mead", "nb  |  nthr  |  time  |  GFLOP/s",
                      transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                      dgemm_time);

    printf("exec|iter: %d|%d\n", exec, iter);

    print_vert("Optimal value pair", z1);
    */


    // Finalize PLASMA
    plasma_finalize();

    // Deallocating memory
    free(A);  free(B);  free(C);

    return 0;
}


// Rightsizes QR factorisation routine 'DGEQRF' for optimal tile size 'nb' and no. of OpenMP threads 'nthr'
int rightsize_dgeqrf(int *range_nb, int tol_nb, int *range_nthr, int tol_nthr, int ib, char hmode,
                     int m, int n) {

    // Rightsizer parameters
    int nb   = 0;
    int nthr = 0;
    int exec = 0;
    int iter = 0;

    // PLASMA parameters
    int lda = imax(1, m);

    // Allocate memory
    double *A = (double*)malloc((size_t)lda*n*sizeof(double));
    assert(A != NULL);

    int seed[] = {0, 0, 0, 1};

    // Initialize matrix A
    lapack_int retval = LAPACKE_dlarnv(1, seed, (size_t)lda*n, A);
    assert(retval == 0);

    printf("Rightsizing DGEQRF...\n");

    // Initialize PLASMA
    plasma_init();

    // Set tuning parameters
    plasma_set(PlasmaTuning, PlasmaDisabled);

    // Set inner block size
    plasma_set(PlasmaIb, ib);

    // Set Householder mode
    if (hmode == 't')
        plasma_set(PlasmaHouseholderMode, PlasmaTreeHouseholder);
    else
        plasma_set(PlasmaHouseholderMode, PlasmaFlatHouseholder);

    // Prepare descriptor for matrix T
    plasma_desc_t T;

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);

    /*
    //--------------------------------------------------------------------------
    printf("Detecting optimal tile size 'nb' with bisection...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  time  |  GFLOP/s\n");

    bisect_dgeqrf(range_nb[0], range_nb[2], &tol_nb, &nb, &exec, &iter,
                 "dgeqrf.bisect.nb", "nb  |  time  |  GFLOP/s",
                  m, n, A, lda, &T,
                  dgeqrf_time_nb);

    printf("nb(exec|iter): %d(%d|%d)\n", nb, exec, iter);

    // @test
    // nb = 704;

    //--------------------------------------------------------------------------
    printf("\nDetecting optimal no. of threads 'nthr' with bisection...");
    printf("\nUsing tile size 'nb=%d'...\n", nb);
    //--------------------------------------------------------------------------
    printf("nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);

    bisect_dgeqrf(range_nthr[0], range_nthr[2], &tol_nthr, &nthr, &exec, &iter,
                 "dgeqrf.bisect.nthr", "nthr  |  time  |  GFLOP/s",
                  m, n, A, lda, &T,
                  dgeqrf_time_nthr);

    printf("nthr(exec|iter): %d(%d|%d)\n", nthr, exec, iter);


    //--------------------------------------------------------------------------
    printf("\nDetecting optimal tile size 'nb' with golden section...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    goldsect_dgeqrf(range_nb[0], range_nb[1], range_nb[2], &tol_nb, &nb, &exec, &iter,
                   "dgeqrf.goldsect.nb", "nb  |  time  |  GFLOP/s",
                    m, n, A, lda, &T,
                    dgeqrf_time_nb);

    printf("nb(exec|iter): %d(%d|%d)\n", nb, exec, iter);

    // @test
    // nb = 232;

    //--------------------------------------------------------------------------
    printf("\nDetecting optimal no. of threads 'nthr' with golden section...");
    printf("\nUsing tile size 'nb=%d'...\n", nb);
    //--------------------------------------------------------------------------
    printf("nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);

    goldsect_dgeqrf(range_nthr[0], range_nthr[1], range_nthr[2], &tol_nthr, &nthr, &exec, &iter,
                   "dgeqrf.goldsect.nthr", "nthr  |  time  |  GFLOP/s",
                    m, n, A, lda, &T,
                    dgeqrf_time_nthr);

    printf("nthr(exec|iter): %d(%d|%d)\n", nthr, exec, iter);
    */


    //--------------------------------------------------------------------------
    printf("\nDetecting optimal tile size 'nb' and no. of threads 'nthr' "
           "with Nelder-Mead algorithm...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Define initial vertices
    vert_t z1 = (vert_t){.x = range_nb[0], .y = range_nthr[0], .f = 0.0, .g = 0.0};
    vert_t z2 = (vert_t){.x = range_nb[1], .y = range_nthr[1], .f = 0.0, .g = 0.0};
    vert_t z3 = (vert_t){.x = range_nb[2], .y = range_nthr[2], .f = 0.0, .g = 0.0};

    nelder_mead_dgeqrf(&z1, &z2, &z3, &tol_nb, &tol_nthr, &exec, &iter,
                      "dgeqrf.nelder.mead", "nb  |  nthr  |  time  |  GFLOP/s",
                       m, n, A, lda, &T,
                       dgeqrf_time);

    printf("exec|iter: %d|%d\n", exec, iter);

    print_vert("Optimal value pair", z1);


    // Finalize PLASMA
    plasma_finalize();

    // Deallocating memory
    free(A);

    return 0;
}


// Rightsizes LU factorisation routine 'DGETRF' for optimal tile size 'nb' and no. of OpenMP threads 'nthr'
int rightsize_dgetrf(int *range_nb, int tol_nb, int *range_nthr, int tol_nthr, int ib, int mtpf,
                     int m, int n) {

    // Rightsizer parameters
    int nb   = 0;
    int nthr = 0;
    int exec = 0;
    int iter = 0;

    // PLASMA parameters
    int lda = imax(1, m);

    // Allocate memory
    double *A = (double*)malloc((size_t)lda*n*sizeof(double));
    assert(A != NULL);

    int *iPiv = (int*)malloc((size_t)m*sizeof(int));
    assert(iPiv != NULL);

    int seed[] = {0, 0, 0, 1};

    // Initialize matrix A
    lapack_int retval = LAPACKE_dlarnv(1, seed, (size_t)lda*n, A);
    assert(retval == 0);

    printf("Rightsizing DGETRF...\n");

    // Initialize PLASMA
    plasma_init();

    // Set tuning parameters
    plasma_set(PlasmaTuning, PlasmaDisabled);

    // Set inner block size
    plasma_set(PlasmaIb, ib);

    // Set maximum no. of threads per panel
    plasma_set(PlasmaNumPanelThreads, mtpf); 

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);


    /*
    //--------------------------------------------------------------------------
    printf("Detecting optimal tile size 'nb' with bisection...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  time  |  GFLOP/s\n");

    bisect_dgetrf(range_nb[0], range_nb[2], &tol_nb, &nb, &exec, &iter,
                 "dgetrf.bisect.nb", "nb  |  time  |  GFLOP/s",
                  m, n, A, lda, iPiv,
                  dgetrf_time_nb);

    printf("nb(exec|iter): %d(%d|%d)\n", nb, exec, iter);

    // @test
    // nb = 704;

    //--------------------------------------------------------------------------
    printf("\nDetecting optimal no. of threads 'nthr' with bisection...");
    printf("\nUsing tile size 'nb=%d'...\n", nb);
    //--------------------------------------------------------------------------
    printf("nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);

    bisect_dgetrf(range_nthr[0], range_nthr[2], &tol_nthr, &nthr, &exec, &iter,
                 "dgetrf.bisect.nthr", "nthr  |  time  |  GFLOP/s",
                  m, n, A, lda, iPiv,
                  dgetrf_time_nthr);

    printf("nthr(exec|iter): %d(%d|%d)\n", nthr, exec, iter);


    //--------------------------------------------------------------------------
    printf("\nDetecting optimal tile size 'nb' with golden section...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    goldsect_dgetrf(range_nb[0], range_nb[1], range_nb[2], &tol_nb, &nb, &exec, &iter,
                   "dgetrf.goldsect.nb", "nb  |  time  |  GFLOP/s",
                    m, n, A, lda, iPiv,
                    dgetrf_time_nb);

    printf("nb(exec|iter): %d(%d|%d)\n", nb, exec, iter);

    // @test
    // nb = 232;

    //--------------------------------------------------------------------------
    printf("\nDetecting optimal no. of threads 'nthr' with golden section...");
    printf("\nUsing tile size 'nb=%d'...\n", nb);
    //--------------------------------------------------------------------------
    printf("nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);

    goldsect_dgetrf(range_nthr[0], range_nthr[1], range_nthr[2], &tol_nthr, &nthr, &exec, &iter,
                   "dgetrf.goldsect.nthr", "nthr  |  time  |  GFLOP/s",
                    m, n, A, lda, iPiv,
                    dgetrf_time_nthr);

    printf("nthr(exec|iter): %d(%d|%d)\n", nthr, exec, iter);
    */


    //--------------------------------------------------------------------------
    printf("\nDetecting optimal tile size 'nb' and no. of threads 'nthr' "
           "with Nelder-Mead algorithm...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Define initial vertices
    vert_t z1 = (vert_t){.x = range_nb[0], .y = range_nthr[0], .f = 0.0, .g = 0.0};
    vert_t z2 = (vert_t){.x = range_nb[1], .y = range_nthr[1], .f = 0.0, .g = 0.0};
    vert_t z3 = (vert_t){.x = range_nb[2], .y = range_nthr[2], .f = 0.0, .g = 0.0};

    nelder_mead_dgetrf(&z1, &z2, &z3, &tol_nb, &tol_nthr, &exec, &iter,
                      "dgetrf.nelder.mead", "nb  |  nthr  |  time  |  GFLOP/s",
                       m, n, A, lda, iPiv,
                       dgetrf_time);

    printf("exec|iter: %d|%d\n", exec, iter);

    print_vert("Optimal value pair", z1);


    // Finalize PLASMA
    plasma_finalize();

    // Deallocating memory
    free(A);  free(iPiv);

    return 0;
}


// Rightsizes Cholesky factorisation routine 'DPOTRF' for optimal tile size 'nb' and no. of OpenMP threads 'nthr'
int rightsize_dpotrf(int *range_nb, int tol_nb, int *range_nthr, int tol_nthr,
                     int n) {

    // Rightsizer parameters
    int nb   = 0;
    int nthr = 0;
    int exec = 0;
    int iter = 0;

    // PLASMA parameters
    plasma_enum_t uplo = PlasmaLower;

    int lda = imax(1, n);

    // Allocate memory
    double *A = (double*)malloc((size_t)lda*n*sizeof(double));
    assert(A != NULL);

    int seed[] = {0, 0, 0, 1};

    // Initialize matrix A
    lapack_int retval = LAPACKE_dlarnv(1, seed, (size_t)lda*n, A);
    assert(retval == 0);

    // Make matrix A symmetric positive definite
    for (int i = 0; i < n; i++) {
        A(i,i) += n;
        for (int j = 0; j < i; j++) {
            A(j,i) = A(i,j);
        }
    }

    printf("Rightsizing DPOTRF...\n");

    // Initialize PLASMA
    plasma_init();

    // Set tuning parameters
    plasma_set(PlasmaTuning, PlasmaDisabled);

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);


    /*
    //--------------------------------------------------------------------------
    printf("Detecting optimal tile size 'nb' with bisection...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  time  |  GFLOP/s\n");

    bisect_dpotrf(range_nb[0], range_nb[2], &tol_nb, &nb, &exec, &iter,
                 "dpotrf.bisect.nb", "nb  |  time  |  GFLOP/s",
                  uplo, n, A, lda,
                  dpotrf_time_nb);

    printf("nb(exec|iter): %d(%d|%d)\n", nb, exec, iter);

    // @test
    // nb = 704;

    //--------------------------------------------------------------------------
    printf("\nDetecting optimal no. of threads 'nthr' with bisection...");
    printf("\nUsing tile size 'nb=%d'...\n", nb);
    //--------------------------------------------------------------------------
    printf("nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);

    bisect_dpotrf(range_nthr[0], range_nthr[2], &tol_nthr, &nthr, &exec, &iter,
                 "dpotrf.bisect.nthr", "nthr  |  time  |  GFLOP/s",
                  uplo, n, A, lda,
                  dpotrf_time_nthr);

    printf("nthr(exec|iter): %d(%d|%d)\n", nthr, exec, iter);


    //--------------------------------------------------------------------------
    printf("\nDetecting optimal tile size 'nb' with golden section...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    goldsect_dpotrf(range_nb[0], range_nb[1], range_nb[2], &tol_nb, &nb, &exec, &iter,
                   "dpotrf.goldsect.nb", "nb  |  time  |  GFLOP/s",
                    uplo, n, A, lda,
                    dpotrf_time_nb);

    printf("nb(exec|iter): %d(%d|%d)\n", nb, exec, iter);

    // @test
    // nb = 232;

    //--------------------------------------------------------------------------
    printf("\nDetecting optimal no. of threads 'nthr' with golden section...");
    printf("\nUsing tile size 'nb=%d'...\n", nb);
    //--------------------------------------------------------------------------
    printf("nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Set tile size
    plasma_set(PlasmaNb, nb);

    // Disable dynamic OpenMP teams
    omp_set_dynamic(0);

    goldsect_dpotrf(range_nthr[0], range_nthr[1], range_nthr[2], &tol_nthr, &nthr, &exec, &iter,
                   "dpotrf.goldsect.nthr", "nthr  |  time  |  GFLOP/s",
                    uplo, n, A, lda,
                    dpotrf_time_nthr);

    printf("nthr(exec|iter): %d(%d|%d)\n", nthr, exec, iter);
    */


    //--------------------------------------------------------------------------
    printf("\nDetecting optimal tile size 'nb' and no. of threads 'nthr' "
           "with Nelder-Mead algorithm...\n");
    //--------------------------------------------------------------------------
    printf("nb  |  nthr  |  time  |  GFLOP/s\n");

    // Reset counters
    exec = 0;  iter = 0;

    // Define initial vertices
    vert_t z1 = (vert_t){.x = range_nb[0], .y = range_nthr[0], .f = 0.0, .g = 0.0};
    vert_t z2 = (vert_t){.x = range_nb[1], .y = range_nthr[1], .f = 0.0, .g = 0.0};
    vert_t z3 = (vert_t){.x = range_nb[2], .y = range_nthr[2], .f = 0.0, .g = 0.0};

    nelder_mead_dpotrf(&z1, &z2, &z3, &tol_nb, &tol_nthr, &exec, &iter,
                      "dpotrf.nelder.mead", "nb  |  nthr  |  time  |  GFLOP/s",
                       uplo, n, A, lda,
                       dpotrf_time);

    printf("exec|iter: %d|%d\n", exec, iter);

    print_vert("Optimal value pair", z1);


    // Finalize PLASMA
    plasma_finalize();

    // Deallocating memory
    free(A);

    return 0;
}


int main(int argc, char *argv[]) {

    // Rightsizer parameters
    int range_nb[3] = {32, 128, 256};
    int tol_nb      =  8;

    int range_nthr[3] = {8, 32, 68};
    int tol_nthr = 2;

    int  const ib    = 64;
    int  const mtpf  =  1;
    char const hmode = 'f';

    int const m = 2000;
    int const n = 2000;
    int const k = 2000;

    double const EN = 2.7182818284590452;
    double const PI = 3.1415926535897932;


    rightsize_dgemm(range_nb, tol_nb, range_nthr, tol_nthr,
                    m, n, k, EN, PI);

    /*
    rightsize_dpotrf(range_nb, tol_nb, range_nthr, tol_nthr,
                     n);

    rightsize_dgetrf(range_nb, tol_nb, range_nthr, tol_nthr, ib, mtpf,
                     m, n);

    rightsize_dgeqrf(range_nb, tol_nb, range_nthr, tol_nthr, ib, hmode,
                     m, n);
    */

    return 0;
}

// EoF: rightsizer.c
