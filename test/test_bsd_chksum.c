/*
 * @brief BSD checksum tester.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 21, 2017
 * @version 0.1
 */

#include <stdio.h>
#include <string.h>

int bsd_chksum_file(FILE *fp) {

    int ch;          // character read
    int chksum = 0;  // checksum mod 2^16

    while ((ch = getc(fp)) != EOF) {

        chksum = (chksum >> 1) + ((chksum & 1) << 15);
        chksum += ch;
        chksum &= 0xffff;  // keep checksum within bounds

    }

    return chksum;
}


int bsd_chksum_str(char *str) {

    int ch;          // character read
    int chksum = 0;  // checksum mod 2^16

    for (int i = 0; i <= strlen(str); i++) {

        chksum = (chksum >> 1) + ((chksum & 1) << 15);
        chksum += str[i];
        chksum &= 0xffff;  // keep checksum within bounds

    }

    return chksum;
}


int main(int argc, char *argv[]) {

    int   nb = 256;
    int   nthr = 8;
    char *fname = "bsd_chksum.txt";
    char  str[32];

    sprintf(str, "%d,%d", nb, nthr);
    int chksum = bsd_chksum_str(str);
    printf("BSD checksum str: %d\n", chksum);

    FILE *fp;

    fp = fopen(fname, "r");
    chksum = bsd_chksum_file(fp);
    fclose(fp);
    printf("BSD checksum file: %d\n", chksum);

}

// #EoF: test_bsd_chksum.c
