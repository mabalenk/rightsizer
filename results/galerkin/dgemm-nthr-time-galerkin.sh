set terminal postscript eps enhanced colour solid 22;  # font "Helvetica, 22";
# set terminal png enhanced;

set lmargin 12;
set key top Left;
set format "%g";
set output "dgemm-nthr-time-galerkin.eps";
set title  "Matrix multiplication (DGEMM) m=n=k=10000, nb=152|128";
set xlabel "No. of threads, nthr";
set ylabel "Execution time, t (s)";
set grid;
# set logscale x;

plot "dgemm.bisect.nthr"   u ($1):($2) smooth unique t "Bisection"      w lp lw 5 ps 1.25, \
     "dgemm.goldsect.nthr" u ($1):($2) smooth unique t "Golden section" w lp lw 5 ps 1.25;
