/*
 * @brief Prototype implementation of Nelder-Mead optmisation method.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 25, 2017
 * @version 0.8
 */

#include "sharp_table.h"


// Vertex definition for functions of two variables, f(x,y); x, y of type double
typedef struct {
    double x;
    double y;
    double f;
} vertex_t;

void print_vertex(char *str, vertex_t v);

int bgw(vertex_t z1, vertex_t z2, vertex_t z3, vertex_t **b, vertex_t **g, vertex_t **w);

int reorder(vertex_t *b, vertex_t *g, vertex_t *w);

int midpoint(vertex_t *b, vertex_t *g, vertex_t *m,
             int (*func)(double, double, double*));

int reflect(vertex_t *m, vertex_t *w, vertex_t *r,
            int (*func)(double, double, double*));

int expand(vertex_t *r, vertex_t *m, vertex_t *e,
           int (*func)(double, double, double*));

int contract(vertex_t *w, vertex_t *m, vertex_t *r, vertex_t *c,
             int (*func)(double, double, double*));

int shrink(vertex_t *b, vertex_t *w, vertex_t *s,
           int (*func)(double, double, double*));

int nelder_mead(vertex_t *b, vertex_t *g, vertex_t *w, int *exec, int *iter,
                int (*func)(double, double, double*));

// -----------------------------------------------------------------------------

// Vertex definition for linear algebra functions, f(x,y); x, y of type int
typedef struct {
    int    x;
    int    y;
    double f;
    double g;
} vert_t;

void print_vert(char *str, vert_t v);

void print_rslt(vert_t v);

int bgw_vert(vert_t z1, vert_t z2, vert_t z3, vert_t **b, vert_t **g, vert_t **w);

int reorder_vert(vert_t *b, vert_t *g, vert_t *w);

// -----------------------------------------------------------------------------

int midpoint_dgemm(vert_t *b, vert_t *g, vert_t *d, int *tol_nb, int *tol_nthr,
                   sharp_t *st, int *exec, double eps,
                   plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                   double alpha, double *A, int lda, double *B, int ldb,
                   double beta, double *C, int ldc,
                   int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                     double*, int, double*, int, double, double*, int,
                                     int, int, plasma_time_t*, double*));

int reflect_dgemm(vert_t *d, vert_t *w, vert_t *r, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                  double alpha, double *A, int lda, double *B, int ldb,
                  double beta, double *C, int ldc,
                  int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                    double*, int, double*, int, double, double*, int,
                                    int, int, plasma_time_t*, double*));

int expand_dgemm(vert_t *r, vert_t *d, vert_t *e, int *tol_nb, int *tol_nthr,
                 sharp_t *st, int *exec, double eps,
                 plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                 double alpha, double *A, int lda, double *B, int ldb,
                 double beta, double *C, int ldc,
                 int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                   double*, int, double*, int, double, double*, int,
                                   int, int, plasma_time_t*, double*));

int contract_dgemm(vert_t *w, vert_t *d, vert_t *r, vert_t *c, int *tol_nb, int *tol_nthr,
                   sharp_t *st, int *exec, double eps,
                   plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                   double alpha, double *A, int lda, double *B, int ldb,
                   double beta, double *C, int ldc,
                   int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                     double*, int, double*, int, double, double*, int,
                                     int, int, plasma_time_t*, double*));

int shrink_dgemm(vert_t *b, vert_t *w, vert_t *s, int *tol_nb, int *tol_nthr,
                 sharp_t *st, int *exec, double eps,
                 plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                 double alpha, double *A, int lda, double *B, int ldb,
                 double beta, double *C, int ldc,
                 int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                   double*, int, double*, int, double, double*, int,
                                   int, int, plasma_time_t*, double*));

int nelder_mead_dgemm(vert_t *b, vert_t *g, vert_t *w,
                      int *tol_nb, int *tol_nthr, int *exec, int *iter, char *fname, char *header,
                      plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                      double alpha, double *A, int lda, double *B, int ldb,
                      double beta, double *C, int ldc,
                      int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                        double*, int, double*, int, double, double*, int,
                                        int, int, plasma_time_t*, double*));

// -----------------------------------------------------------------------------

int midpoint_dpotrf(vert_t *b, vert_t *g, vert_t *d, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    plasma_enum_t uplo, int n, double *A, int lda,
                    int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                       int, int, plasma_time_t*, double*));

int reflect_dpotrf(vert_t *d, vert_t *w, vert_t *r, int *tol_nb, int *tol_nthr,
                   sharp_t *st, int *exec, double eps,
                   plasma_enum_t uplo, int n, double *A, int lda,
                   int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                      int, int, plasma_time_t*, double*));

int expand_dpotrf(vert_t *r, vert_t *d, vert_t *e, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  plasma_enum_t uplo, int n, double *A, int lda,
                  int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                     int, int, plasma_time_t*, double*));

int contract_dpotrf(vert_t *w, vert_t *d, vert_t *r, vert_t *c, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    plasma_enum_t uplo, int n, double *A, int lda,
                    int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                       int, int, plasma_time_t*, double*));

int shrink_dpotrf(vert_t *b, vert_t *w, vert_t *s, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  plasma_enum_t uplo, int n, double *A, int lda,
                  int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                     int, int, plasma_time_t*, double*));

int nelder_mead_dpotrf(vert_t *b, vert_t *g, vert_t *w,
                       int *tol_nb, int *tol_nthr, int *exec, int *iter, char *fname, char *header,
                       plasma_enum_t uplo, int n, double *A, int lda,
                       int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                          int, int, plasma_time_t*, double*));

// -----------------------------------------------------------------------------

int midpoint_dgetrf(vert_t *b, vert_t *g, vert_t *d, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    int m, int n, double *A, int lda, int *iPiv,
                    int (*dgetrf_time)(int, int, double*, int, int*,
                                       int, int, plasma_time_t*, double*));

int reflect_dgetrf(vert_t *d, vert_t *w, vert_t *r, int *tol_nb, int *tol_nthr,
                   sharp_t *st, int *exec, double eps,
                   int m, int n, double *A, int lda, int *iPiv,
                   int (*dgetrf_time)(int, int, double*, int, int*,
                                      int, int, plasma_time_t*, double*));

int expand_dgetrf(vert_t *r, vert_t *d, vert_t *e, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  int m, int n, double *A, int lda, int *iPiv,
                  int (*dgetrf_time)(int, int, double*, int, int*,
                                     int, int, plasma_time_t*, double*));

int contract_dgetrf(vert_t *w, vert_t *d, vert_t *r, vert_t *c, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    int m, int n, double *A, int lda, int *iPiv,
                    int (*dgetrf_time)(int, int, double*, int, int*,
                                       int, int, plasma_time_t*, double*));

int shrink_dgetrf(vert_t *b, vert_t *w, vert_t *s, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  int m, int n, double *A, int lda, int *iPiv,
                  int (*dgetrf_time)(int, int, double*, int, int*,
                                     int, int, plasma_time_t*, double*));

int nelder_mead_dgetrf(vert_t *b, vert_t *g, vert_t *w,
                       int *tol_nb, int *tol_nthr, int *exec, int *iter, char *fname, char *header,
                       int m, int n, double *A, int lda, int *iPiv,
                       int (*dgetrf_time)(int, int, double*, int, int*,
                                          int, int, plasma_time_t*, double*));

// -----------------------------------------------------------------------------

int midpoint_dgeqrf(vert_t *b, vert_t *g, vert_t *d, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    int m, int n, double *A, int lda, plasma_desc_t *T,
                    int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                       int, int, plasma_time_t*, double*));

int reflect_dgeqrf(vert_t *d, vert_t *w, vert_t *r, int *tol_nb, int *tol_nthr,
                   sharp_t *st, int *exec, double eps,
                   int m, int n, double *A, int lda, plasma_desc_t *T,
                   int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                      int, int, plasma_time_t*, double*));

int expand_dgeqrf(vert_t *r, vert_t *d, vert_t *e, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  int m, int n, double *A, int lda, plasma_desc_t *T,
                  int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                     int, int, plasma_time_t*, double*));

int contract_dgeqrf(vert_t *w, vert_t *d, vert_t *r, vert_t *c, int *tol_nb, int *tol_nthr,
                    sharp_t *st, int *exec, double eps,
                    int m, int n, double *A, int lda, plasma_desc_t *T,
                    int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                       int, int, plasma_time_t*, double*));

int shrink_dgeqrf(vert_t *b, vert_t *w, vert_t *s, int *tol_nb, int *tol_nthr,
                  sharp_t *st, int *exec, double eps,
                  int m, int n, double *A, int lda, plasma_desc_t *T,
                  int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                     int, int, plasma_time_t*, double*));

int nelder_mead_dgeqrf(vert_t *b, vert_t *g, vert_t *w,
                       int *tol_nb, int *tol_nthr, int *exec, int *iter, char *fname, char *header,
                       int m, int n, double *A, int lda, plasma_desc_t *T,
                       int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                          int, int, plasma_time_t*, double*));

// EoF: nelder_mead.h
