set terminal postscript eps enhanced colour solid 22;  # font "Helvetica, 22";
# set terminal png enhanced;

set lmargin 12;
set key top Left;
set format "%g";
set output "dgetrf-nb-time-deimos.eps";
set title  "LU factorisation (DGETRF) m=n=k=30000, n_{thr}=68";
set xlabel "Tile size, nb";
set ylabel "Execution time, t (s)";
set grid;
# set logscale x;

plot "dgetrf.bisect.nb"   u ($1):($2) smooth unique t "Bisection"      w lp lw 5 ps 1.25, \
     "dgetrf.goldsect.nb" u ($1):($2) smooth unique t "Golden section" w lp lw 5 ps 1.25;
