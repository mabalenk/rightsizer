/*
 * @brief Bisection and golden section implementation.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 25, 2017
 * @version 0.15
 */

#include "test.h"
#include "plasma.h"
#include "core_lapack.h"
#include "hash_table.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define SHIFT2(a,b,c)   (a)=(b); (b)=(c);
#define SHIFT3(a,b,c,d) (a)=(b); (b)=(c); (c)=(d);


// Finds nearest multiple of m for given n
int nm(int n, int m) {

    return (n == 0) ? 1 : round((double)n/(double)m) * m;
}


// Finds minimum value in given array
double fmin_arr(double *arr, int len) {

    double fmin = arr[0];

    for (int i = 1; i < len; i++) {
        if (arr[i] < fmin) {
            fmin = arr[i];
        }
    }

    return fmin;
}


// Calculates no. of iterations of bisection algorithm
int bisect_niter(int a, int b, int x) {

    return ceil(log((b-a)/x) / log(2));
}


double sign(double a, double b) {

    return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);
}


// Searches in downhill direction and returns new points that bracket minimum of function
void bracket_min_dgemm(int *xa, int *xb, int *xc, int *tol, hash_t *h, int *exec,
                       plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                       double alpha, double *A, int lda, double *B, int ldb,  double beta, double *C, int ldc,
                       int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                         double*, int, double*, int, double, double*, int,
                                         int, plasma_time_t*, double*)) {

    int    const GLIMIT = 100;
    double const GOLD   = 1.618034;
    double const TINY   = 1.0e-20;

    int    xu;
    double ulim, r, q;

    double fa, fb, fc, fu;
    double ga, gb, gc, gu;

    // printf("Bracketing has been activated...\n");

    (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                  *xa, &fa, &ga);
    *exec += 1;

    (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                  *xb, &fb, &gb);
    *exec += 1;

    // Store xy|fy|gy triplets in hash table
    hash_ins(h, *xa, fa, ga);
    hash_ins(h, *xb, fb, gb);

    printf("%.d\t%.3f\t%.3f\n", *xa, fa, ga);
    printf("%.d\t%.3f\t%.3f\n", *xb, fb, gb);

    // Switch roles of a and b to go downhill in direction from a to b
    if (fb > fa) {
        SHIFT3(xu,*xa,*xb,xu);
        SHIFT3(fu, fa, fb,fu);
    }

    // Initial guess for c
    *xc = nm((*xb) + GOLD*(*xb - *xa), *tol);
    (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                  *xc, &fc, &gc);
    *exec += 1;

    // Store xc|fc|gc triplet in hash table
    hash_ins(h, *xc, fc, gc);

    printf("%.d\t%.3f\t%.3f\n", *xc, fc, gc);

    // Repeat until minimum is bracketed
    while (fb > fc) {

        r = (*xb-*xa)*(fb-fc);
        q = (*xb-*xc)*(fb-fa);

        xu = nm((*xb)-((*xb-*xc)*q-(*xb-*xa)*r)/(2.0*sign(fmax(fabs(q-r),TINY),q-r)), *tol);

        ulim = nm((*xb)+GLIMIT*(*xc-*xb), *tol);

        // Parabolic u is betw b and c
        if ((*xb-xu)*(xu-*xc) > 0.0) {

            (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                          xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);

            // Minimum is betw b and c
            if (fu < fc) {
                SHIFT2(*xa,*xb,xu);
                SHIFT2( fa, fb,fu);

                // @test
                // printf("Bracketing is completed...\n");

                return;
            }
            // Minimum betw a and u
            else if (fu > fb) {
                *xc = xu;  fc = fu;
            }

            // Reject parabolic u. Apply default magnification.
            xu = nm((*xc) + GOLD*(*xc-*xb), *tol);

            (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                          xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Parabolic fit is betw c and its allowed lim
        else if ((*xc-xu)*(xu-ulim) > 0.0) {

            (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                          xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);

            if (fu < fc) {
                SHIFT3(*xb,*xc,xu,nm(*xc+GOLD*(*xc-*xb), *tol));
                SHIFT2( fb, fc,fu);

                (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                              xu, &fu, &gu);
                *exec += 1;

                // Store xu|fu|gu triplet in hash table
                hash_ins(h, xu, fu, gu);

                printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
            }
        }

        // Limit parabolic u to maximum allowed value
        else if ((xu-ulim)*(ulim-*xc) >= 0.0) {
            xu = ulim;
            (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                          xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Reject parabolic u. Apply default magnification.
        else {
            xu = nm((*xc)+GOLD*(*xc-*xb), *tol);
            (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                          xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Eliminate oldest point and cont
        SHIFT3(*xa,*xb,*xc,xu);
        SHIFT3( fa, fb, fc,fu);
    }

    // printf("Bracketing is completed...\n");
}


// Searches in downhill direction and returns new points that bracket minimum of function
void bracket_min_dgeqrf(int *xa, int *xb, int *xc, int *tol, hash_t *h, int *exec,
                        int m, int n, double *A, int lda, plasma_desc_t *T,
                        int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                           int, plasma_time_t*, double*)) {

    int    const GLIMIT = 100;
    double const GOLD   = 1.618034;
    double const TINY   = 1.0e-20;

    int    xu;
    double ulim, r, q;

    double fa, fb, fc, fu;
    double ga, gb, gc, gu;

    // printf("Bracketing has been activated...\n");

    (*dgeqrf_time)(m, n, A, lda, T,
                   *xa, &fa, &ga);
    *exec += 1;

    (*dgeqrf_time)(m, n, A, lda, T,
                   *xb, &fb, &gb);
    *exec += 1;

    // Store xy|fy|gy triplets in hash table
    hash_ins(h, *xa, fa, ga);
    hash_ins(h, *xb, fb, gb);

    printf("%.d\t%.3f\t%.3f\n", *xa, fa, ga);
    printf("%.d\t%.3f\t%.3f\n", *xb, fb, gb);

    // Switch roles of a and b to go downhill in direction from a to b
    if (fb > fa) {
        SHIFT3(xu,*xa,*xb,xu);
        SHIFT3(fu, fa, fb,fu);
    }

    // Initial guess for c
    *xc = nm((*xb) + GOLD*(*xb - *xa), *tol);
    (*dgeqrf_time)(m, n, A, lda, T,
                   *xc, &fc, &gc);
    *exec += 1;

    // Store xc|fc|gc triplet in hash table
    hash_ins(h, *xc, fc, gc);

    printf("%.d\t%.3f\t%.3f\n", *xc, fc, gc);

    // Repeat until minimum is bracketed
    while (fb > fc) {

        r = (*xb-*xa)*(fb-fc);
        q = (*xb-*xc)*(fb-fa);

        xu = nm((*xb)-((*xb-*xc)*q-(*xb-*xa)*r)/(2.0*sign(fmax(fabs(q-r),TINY),q-r)), *tol);

        ulim = nm((*xb)+GLIMIT*(*xc-*xb), *tol);

        // Parabolic u is betw b and c
        if ((*xb-xu)*(xu-*xc) > 0.0) {

            (*dgeqrf_time)(m, n, A, lda, T,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);

            // Minimum is betw b and c
            if (fu < fc) {
                SHIFT2(*xa,*xb,xu);
                SHIFT2( fa, fb,fu);

                // @test
                // printf("Bracketing is completed...\n");

                return;
            }
            // Minimum betw a and u
            else if (fu > fb) {
                *xc = xu;  fc = fu;
            }

            // Reject parabolic u. Apply default magnification.
            xu = nm((*xc) + GOLD*(*xc-*xb), *tol);

            (*dgeqrf_time)(m, n, A, lda, T,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Parabolic fit is betw c and its allowed lim
        else if ((*xc-xu)*(xu-ulim) > 0.0) {

            (*dgeqrf_time)(m, n, A, lda, T,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);

            if (fu < fc) {
                SHIFT3(*xb,*xc,xu,nm(*xc+GOLD*(*xc-*xb), *tol));
                SHIFT2( fb, fc,fu);

                (*dgeqrf_time)(m, n, A, lda, T,
                               xu, &fu, &gu);
                *exec += 1;

                // Store xu|fu|gu triplet in hash table
                hash_ins(h, xu, fu, gu);

                printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
            }
        }

        // Limit parabolic u to maximum allowed value
        else if ((xu-ulim)*(ulim-*xc) >= 0.0) {
            xu = ulim;
            (*dgeqrf_time)(m, n, A, lda, T,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Reject parabolic u. Apply default magnification.
        else {
            xu = nm((*xc)+GOLD*(*xc-*xb), *tol);
            (*dgeqrf_time)(m, n, A, lda, T,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Eliminate oldest point and cont
        SHIFT3(*xa,*xb,*xc,xu);
        SHIFT3( fa, fb, fc,fu);
    }

    // printf("Bracketing is completed...\n");
}


// Searches in downhill direction and returns new points that bracket minimum of function
void bracket_min_dgetrf(int *xa, int *xb, int *xc, int *tol, hash_t *h, int *exec,
                        int m, int n, double *A, int lda, int *iPiv,
                        int (*dgetrf_time)(int, int, double*, int, int*,
                                           int, plasma_time_t*, double*)) {

    int    const GLIMIT = 100;
    double const GOLD   = 1.618034;
    double const TINY   = 1.0e-20;

    int    xu;
    double ulim, r, q;

    double fa, fb, fc, fu;
    double ga, gb, gc, gu;

    // printf("Bracketing has been activated...\n");

    (*dgetrf_time)(m, n, A, lda, iPiv,
                   *xa, &fa, &ga);
    *exec += 1;

    (*dgetrf_time)(m, n, A, lda, iPiv,
                   *xb, &fb, &gb);
    *exec += 1;

    // Store xy|fy|gy triplet in hash table
    hash_ins(h, *xa, fa, ga);
    hash_ins(h, *xb, fb, gb);

    printf("%.d\t%.3f\t%.3f\n", *xa, fa, ga);
    printf("%.d\t%.3f\t%.3f\n", *xb, fb, gb);

    // Switch roles of a and b to go downhill in direction from a to b
    if (fb > fa) {
        SHIFT3(xu,*xa,*xb,xu);
        SHIFT3(fu, fa, fb,fu);
    }

    // Initial guess for c
    *xc = nm((*xb) + GOLD*(*xb - *xa), *tol);
    (*dgetrf_time)(m, n, A, lda, iPiv,
                   *xc, &fc, &gc);
    *exec += 1;

    // Store xc|fc|gc triplet in hash table
    hash_ins(h, *xc, fc, gc);

    printf("%.d\t%.3f\t%.3f\n", *xc, fc, gc);

    // Repeat until minimum is bracketed
    while (fb > fc) {

        r = (*xb-*xa)*(fb-fc);
        q = (*xb-*xc)*(fb-fa);

        xu = nm((*xb)-((*xb-*xc)*q-(*xb-*xa)*r)/(2.0*sign(fmax(fabs(q-r),TINY),q-r)), *tol);

        ulim = nm((*xb)+GLIMIT*(*xc-*xb), *tol);

        // Parabolic u is betw b and c
        if ((*xb-xu)*(xu-*xc) > 0.0) {

            (*dgetrf_time)(m, n, A, lda, iPiv,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);

            // Minimum is betw b and c
            if (fu < fc) {
                SHIFT2(*xa,*xb,xu);
                SHIFT2( fa, fb,fu);

                // @test
                // printf("Bracketing is completed...\n");

                return;
            }
            // Minimum betw a and u
            else if (fu > fb) {
                *xc = xu;  fc = fu;
            }

            // Reject parabolic u. Apply default magnification.
            xu = nm((*xc) + GOLD*(*xc-*xb), *tol);

            (*dgetrf_time)(m, n, A, lda, iPiv,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Parabolic fit is betw c and its allowed lim
        else if ((*xc-xu)*(xu-ulim) > 0.0) {

            (*dgetrf_time)(m, n, A, lda, iPiv,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);

            if (fu < fc) {
                SHIFT3(*xb,*xc,xu,nm(*xc+GOLD*(*xc-*xb), *tol));
                SHIFT2( fb, fc,fu);

                (*dgetrf_time)(m, n, A, lda, iPiv,
                               xu, &fu, &gu);
                *exec += 1;

                // Store xu|fu|gu triplet in hash table
                hash_ins(h, xu, fu, gu);

                printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
            }
        }

        // Limit parabolic u to maximum allowed value
        else if ((xu-ulim)*(ulim-*xc) >= 0.0) {
            xu = ulim;
            (*dgetrf_time)(m, n, A, lda, iPiv,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Reject parabolic u. Apply default magnification.
        else {
            xu = nm((*xc)+GOLD*(*xc-*xb), *tol);
            (*dgetrf_time)(m, n, A, lda, iPiv,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Eliminate oldest point and cont
        SHIFT3(*xa,*xb,*xc,xu);
        SHIFT3( fa, fb, fc,fu);
    }

    // printf("Bracketing is completed...\n");
}


// Searches in downhill direction and returns new points that bracket minimum of function
void bracket_min_dpotrf(int *xa, int *xb, int *xc, int *tol, hash_t *h, int *exec,
                        plasma_enum_t uplo, int n, double *A, int lda,
                        int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                           int, plasma_time_t*, double*)) {

    int    const GLIMIT = 100;
    double const GOLD   = 1.618034;
    double const TINY   = 1.0e-20;

    int    xu;
    double ulim, r, q;

    double fa, fb, fc, fu;
    double ga, gb, gc, gu;

    // printf("Bracketing has been activated...\n");

    (*dpotrf_time)(uplo, n, A, lda,
                   *xa, &fa, &ga);
    *exec += 1;

    (*dpotrf_time)(uplo, n, A, lda,
                   *xb, &fb, &gb);
    *exec += 1;

    // Store xy|fy|gy triplets in hash table
    hash_ins(h, *xa, fa, ga);
    hash_ins(h, *xb, fb, gb);

    printf("%.d\t%.3f\t%.3f\n", *xa, fa, ga);
    printf("%.d\t%.3f\t%.3f\n", *xb, fb, gb);

    // Switch roles of a and b to go downhill in direction from a to b
    if (fb > fa) {
        SHIFT3(xu,*xa,*xb,xu);
        SHIFT3(fu, fa, fb,fu);
    }

    // Initial guess for c
    *xc = nm((*xb) + GOLD*(*xb - *xa), *tol);
    (*dpotrf_time)(uplo, n, A, lda,
                   *xc, &fc, &gc);
    *exec += 1;

    // Store xc|fc|gc triplet in hash table
    hash_ins(h, *xc, fc, gc);

    printf("%.d\t%.3f\t%.3f\n", *xc, fc, gc);

    // Repeat until minimum is bracketed
    while (fb > fc) {

        r = (*xb-*xa)*(fb-fc);
        q = (*xb-*xc)*(fb-fa);

        xu = nm((*xb)-((*xb-*xc)*q-(*xb-*xa)*r)/(2.0*sign(fmax(fabs(q-r),TINY),q-r)), *tol);

        ulim = nm((*xb)+GLIMIT*(*xc-*xb), *tol);

        // Parabolic u is betw b and c
        if ((*xb-xu)*(xu-*xc) > 0.0) {

            (*dpotrf_time)(uplo, n, A, lda,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);

            // Minimum is betw b and c
            if (fu < fc) {
                SHIFT2(*xa,*xb,xu);
                SHIFT2( fa, fb,fu);

                // @test
                // printf("Bracketing is completed...\n");

                return;
            }
            // Minimum betw a and u
            else if (fu > fb) {
                *xc = xu;  fc = fu;
            }

            // Reject parabolic u. Apply default magnification.
            xu = nm((*xc) + GOLD*(*xc-*xb), *tol);

            (*dpotrf_time)(uplo, n, A, lda,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Parabolic fit is betw c and its allowed lim
        else if ((*xc-xu)*(xu-ulim) > 0.0) {

            (*dpotrf_time)(uplo, n, A, lda,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);

            if (fu < fc) {
                SHIFT3(*xb,*xc,xu,nm(*xc+GOLD*(*xc-*xb), *tol));
                SHIFT2( fb, fc,fu);

                (*dpotrf_time)(uplo, n, A, lda,
                               xu, &fu, &gu);
                *exec += 1;

                // Store xu|fu|gu triplet in hash table
                hash_ins(h, xu, fu, gu);

                printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
            }
        }

        // Limit parabolic u to maximum allowed value
        else if ((xu-ulim)*(ulim-*xc) >= 0.0) {
            xu = ulim;
            (*dpotrf_time)(uplo, n, A, lda,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Reject parabolic u. Apply default magnification.
        else {
            xu = nm((*xc)+GOLD*(*xc-*xb), *tol);
            (*dpotrf_time)(uplo, n, A, lda,
                           xu, &fu, &gu);
            *exec += 1;

            // Store xu|fu|gu triplet in hash table
            hash_ins(h, xu, fu, gu);

            printf("%.d\t%.3f\t%.3f\n", xu, fu, gu);
        }

        // Eliminate oldest point and cont
        SHIFT3(*xa,*xb,*xc,xu);
        SHIFT3( fa, fb, fc,fu);
    }

    // printf("Bracketing is completed...\n");
}


// Optimises DGEMM routine for best tile size 'nb' or no. of OpenMP threads 'nthr' using bisection algorithm
int bisect_dgemm(int a, int b, int *tol, int *nb, int *exec, int *iter, char *fname, char *header,
                 plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                 double alpha, double *A, int lda, double *B, int ldb, double beta, double *C, int ldc,
                 int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                   double*, int, double*, int, double, double*, int,
                                   int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    int xa = a;
    int xb = b;
    int xm = nm((a+b)/2, *tol);

    double fa, fb, fm, ga, gb, gm;

    *exec = 0;  *iter = 0;

    // Calculate size of hash table (no. of function executions)
    int const K = 2*bisect_niter(a, b, *tol) + 3;

    // Create hash table to store nb|time value triplets
    hash_t *h = hash_new(K);

    (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                  xa, &fa, &ga);
    *exec += 1;

    (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                  xm, &fm, &gm);
    *exec += 1;

    (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                  xb, &fb, &gb);
    *exec += 1;

    // Store xy|fy|gy triplets in hash table
    hash_ins(h, xa, fa, ga);
    hash_ins(h, xm, fm, gm);
    hash_ins(h, xb, fb, gb);

    printf("%.d\t%.3f\t%.3f\n", xa, fa, ga);
    printf("%.d\t%.3f\t%.3f\n", xm, fm, gm);
    printf("%.d\t%.3f\t%.3f\n", xb, fb, gb);

    do {
        int xl = nm((xa+xm)/2, *tol);
        int xr = nm((xm+xb)/2, *tol);

        double fl = hash_lookup(h, xl, 't');
        double fr = hash_lookup(h, xr, 't');

        double gl = 0.0, gr = 0.0;

        // Execute function, if experiment with this 'nb' value is not present
        if (fabs(fl-0.0) <= eps) {

            // printf("Performing experiment with nb=%d\n", xl);

            (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                          xl, &fl, &gl);
            *exec += 1;

            // Store xl|fl|gl triplet in hash table
            hash_ins(h, xl, fl, gl);

            printf("%.d\t%.3f\t%.3f\n", xl, fl, gl);
        }
        
        // Execute function, if experiment with this nb value is not present
        if (fabs(fr-0.0) <= eps) {

            // printf("Performing experiment with nb=%d\n", xr);

            (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                          xr, &fr, &gr);
            *exec += 1;

            // Store xr|fr|gr triplet in hash table
            hash_ins(h, xr, fr, gr);

            printf("%.d\t%.3f\t%.3f\n", xr, fr, gr);
        }

        plasma_time_t arr[] = {fa, fb, fm, fl, fr};

        plasma_time_t fmin = fmin_arr(arr, 5);
        // printf("fmin: %.3f\n", fmin);

        if ( (fabs(fmin-fa) <= eps) || (fabs(fmin-fl) <= eps) ) {
            // printf("fmin = fa OR fmin = fl\n");
            SHIFT2(xb,xm,xl); SHIFT2(fb,fm,fl);
        }
        else if (fabs(fmin-fm) <= eps) {
            // printf("fmin = fm\n");
            xa = xl; xb = xr; fa = fl; fb = fr;
        }
        else if ( (fabs(fmin-fr) <= eps) || (fabs(fmin-fb) <= eps) ) {
            // printf("fmin = fr OR fmin = fb\n");
            SHIFT2(xa,xm,xr); SHIFT2(fa,fm,fr);
        }

        *iter += 1;

        // printf("|xb-xa| ?> tol: %d\t%d\n", abs(xb-xa), *tol);

    } while (abs(xb-xa) > *tol);

    *nb = xm;

    // Print out full hash table
    // hash_print(h);

    // Save full hash table into file
    hash_save(h, fname, header);

    // Destroy hash table
    hash_free(h);

    return 0;
}


// Optimises DPOTRF routine for best tile size 'nb' or no. of OpenMP threads 'nthr' using bisection algorithm
int bisect_dpotrf(int a, int b, int *tol, int *nb, int *exec, int *iter, char *fname, char *header,
                  plasma_enum_t uplo, int n, double *A, int lda,
                  int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                     int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    int xa = a;
    int xb = b;
    int xm = nm((a+b)/2, *tol);

    double fa, fb, fm, ga, gb, gm;

    *exec = 0;  *iter = 0;

    // Calculate size of hash table (no. of function executions)
    int const K = 2*bisect_niter(a, b, *tol) + 3;

    // Create hash table to store nb|time value triplets
    hash_t *h = hash_new(K);

    (*dpotrf_time)(uplo, n, A, lda,
                   xa, &fa, &ga);
    *exec += 1;

    (*dpotrf_time)(uplo, n, A, lda,
                   xm, &fm, &gm);
    *exec += 1;

    (*dpotrf_time)(uplo, n, A, lda,
                   xb, &fb, &gb);
    *exec += 1;

    // Store xy|fy|gy triplets in hash table
    hash_ins(h, xa, fa, ga);
    hash_ins(h, xm, fm, gm);
    hash_ins(h, xb, fb, gb);

    printf("%.d\t%.3f\t%.3f\n", xa, fa, ga);
    printf("%.d\t%.3f\t%.3f\n", xm, fm, gm);
    printf("%.d\t%.3f\t%.3f\n", xb, fb, gb);

    do {
        int xl = nm((xa+xm)/2, *tol);
        int xr = nm((xm+xb)/2, *tol);

        double fl = hash_lookup(h, xl, 't');
        double fr = hash_lookup(h, xr, 't');

        double gl = 0.0, gr = 0.0;

        // Execute function, if experiment with this 'nb' value is not present
        if (fabs(fl-0.0) <= eps) {

            // printf("Performing experiment with nb=%d\n", xl);

            (*dpotrf_time)(uplo, n, A, lda,
                           xl, &fl, &gl);
            *exec += 1;

            // Store xl|fl|gl triplet in hash table
            hash_ins(h, xl, fl, gl);

            printf("%.d\t%.3f\t%.3f\n", xl, fl, gl);
        }
        
        // Execute function, if experiment with this nb value is not present
        if (fabs(fr-0.0) <= eps) {

            // printf("Performing experiment with nb=%d\n", xr);

            (*dpotrf_time)(uplo, n, A, lda,
                           xr, &fr, &gr);
            *exec += 1;

            // Store xr|fr|gr triplet in hash table
            hash_ins(h, xr, fr, gr);

            printf("%.d\t%.3f\t%.3f\n", xr, fr, gr);
        }

        plasma_time_t arr[] = {fa, fb, fm, fl, fr};

        plasma_time_t fmin = fmin_arr(arr, 5);
        // printf("fmin: %.3f\n", fmin);

        if ( (fabs(fmin-fa) <= eps) || (fabs(fmin-fl) <= eps) ) {
            // printf("fmin = fa OR fmin = fl\n");
            SHIFT2(xb,xm,xl); SHIFT2(fb,fm,fl);
        }
        else if (fabs(fmin-fm) <= eps) {
            // printf("fmin = fm\n");
            xa = xl; xb = xr; fa = fl; fb = fr;
        }
        else if ( (fabs(fmin-fr) <= eps) || (fabs(fmin-fb) <= eps) ) {
            // printf("fmin = fr OR fmin = fb\n");
            SHIFT2(xa,xm,xr); SHIFT2(fa,fm,fr);
        }

        *iter += 1;

        // printf("|xb-xa| ?> tol: %d\t%d\n", abs(xb-xa), *tol);

    } while (abs(xb-xa) > *tol);

    *nb = xm;

    // Print out full hash table
    // hash_print(h);

    // Save full hash table into file
    hash_save(h, fname, header);

    // Destroy hash table
    hash_free(h);

    return 0;
}


// Optimises DGEQRF routine for best tile size 'nb' or no. of OpenMP threads 'nthr' using bisection algorithm
int bisect_dgeqrf(int a, int b, int *tol, int *nb, int *exec, int *iter, char *fname, char *header,
                  int m, int n, double *A, int lda, plasma_desc_t *T,
                  int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                     int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    int xa = a;
    int xb = b;
    int xm = nm((a+b)/2, *tol);

    double fa, fb, fm, ga, gb, gm;

    *exec = 0;  *iter = 0;

    // Calculate size of hash table (no. of function executions)
    int const K = 2*bisect_niter(a, b, *tol) + 3;

    // Create hash table to store nb|time value triplets
    hash_t *h = hash_new(K);

    (*dgeqrf_time)(m, n, A, lda, T,
                   xa, &fa, &ga);
    *exec += 1;

    (*dgeqrf_time)(m, n, A, lda, T,
                   xm, &fm, &gm);
    *exec += 1;

    (*dgeqrf_time)(m, n, A, lda, T,
                   xb, &fb, &gb);
    *exec += 1;

    // Store xy|fy|gy triplets in hash table
    hash_ins(h, xa, fa, ga);
    hash_ins(h, xm, fm, gm);
    hash_ins(h, xb, fb, gb);

    printf("%.d\t%.3f\t%.3f\n", xa, fa, ga);
    printf("%.d\t%.3f\t%.3f\n", xm, fm, gm);
    printf("%.d\t%.3f\t%.3f\n", xb, fb, gb);

    do {
        int xl = nm((xa+xm)/2, *tol);
        int xr = nm((xm+xb)/2, *tol);

        double fl = hash_lookup(h, xl, 't');
        double fr = hash_lookup(h, xr, 't');

        double gl = 0.0, gr = 0.0;

        // Execute function, if experiment with this 'nb' value is not present
        if (fabs(fl-0.0) <= eps) {

            // printf("Performing experiment with nb=%d\n", xl);

            (*dgeqrf_time)(m, n, A, lda, T,
                           xl, &fl, &gl);
            *exec += 1;

            // Store xl|fl|gl triplet in hash table
            hash_ins(h, xl, fl, gl);

            printf("%.d\t%.3f\t%.3f\n", xl, fl, gl);
        }
        
        // Execute function, if experiment with this nb value is not present
        if (fabs(fr-0.0) <= eps) {

            // printf("Performing experiment with nb=%d\n", xr);

            (*dgeqrf_time)(m, n, A, lda, T,
                           xr, &fr, &gr);
            *exec += 1;

            // Store xr|fr|gr triplet in hash table
            hash_ins(h, xr, fr, gr);

            printf("%.d\t%.3f\t%.3f\n", xr, fr, gr);
        }

        plasma_time_t arr[] = {fa, fb, fm, fl, fr};

        plasma_time_t fmin = fmin_arr(arr, 5);
        // printf("fmin: %.3f\n", fmin);

        if ( (fabs(fmin-fa) <= eps) || (fabs(fmin-fl) <= eps) ) {
            // printf("fmin = fa OR fmin = fl\n");
            SHIFT2(xb,xm,xl); SHIFT2(fb,fm,fl);
        }
        else if (fabs(fmin-fm) <= eps) {
            // printf("fmin = fm\n");
            xa = xl; xb = xr; fa = fl; fb = fr;
        }
        else if ( (fabs(fmin-fr) <= eps) || (fabs(fmin-fb) <= eps) ) {
            // printf("fmin = fr OR fmin = fb\n");
            SHIFT2(xa,xm,xr); SHIFT2(fa,fm,fr);
        }

        *iter += 1;

        // printf("|xb-xa| ?> tol: %d\t%d\n", abs(xb-xa), *tol);

    } while (abs(xb-xa) > *tol);

    *nb = xm;

    // Print out full hash table
    // hash_print(h);

    // Save full hash table into file
    hash_save(h, fname, header);

    // Destroy hash table
    hash_free(h);

    return 0;
}


// Optimises DGETRF routine for best tile size 'nb' or no. of OpenMP threads 'nthr' using bisection algorithm
int bisect_dgetrf(int a, int b, int *tol, int *nb, int *exec, int *iter, char *fname, char *header,
                  int m, int n, double *A, int lda, int *iPiv,
                  int (*dgetrf_time)(int, int, double*, int, int*,
                                     int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    int xa = a;
    int xb = b;
    int xm = nm((a+b)/2, *tol);

    double fa, fb, fm, ga, gb, gm;

    *exec = 0;  *iter = 0;

    // Calculate size of hash table (no. of function executions)
    int const K = 2*bisect_niter(a, b, *tol) + 3;

    // Create hash table to store nb|time value triplets
    hash_t *h = hash_new(K);

    (*dgetrf_time)(m, n, A, lda, iPiv,
                   xa, &fa, &ga);
    *exec += 1;

    (*dgetrf_time)(m, n, A, lda, iPiv,
                   xm, &fm, &gm);
    *exec += 1;

    (*dgetrf_time)(m, n, A, lda, iPiv,
                   xb, &fb, &gb);
    *exec += 1;

    // Store xy|fy|gy triplets in hash table
    hash_ins(h, xa, fa, ga);
    hash_ins(h, xm, fm, gm);
    hash_ins(h, xb, fb, gb);

    printf("%.d\t%.3f\t%.3f\n", xa, fa, ga);
    printf("%.d\t%.3f\t%.3f\n", xm, fm, gm);
    printf("%.d\t%.3f\t%.3f\n", xb, fb, gb);

    do {
        int xl = nm((xa+xm)/2, *tol);
        int xr = nm((xm+xb)/2, *tol);

        double fl = hash_lookup(h, xl, 't');
        double fr = hash_lookup(h, xr, 't');

        double gl = 0.0, gr = 0.0;

        // Execute function, if experiment with this 'nb' value is not present
        if (fabs(fl-0.0) <= eps) {

            // printf("Performing experiment with nb=%d\n", xl);

            (*dgetrf_time)(m, n, A, lda, iPiv,
                           xl, &fl, &gl);
            *exec += 1;

            // Store xl|fl|gl triplet in hash table
            hash_ins(h, xl, fl, gl);

            printf("%.d\t%.3f\t%.3f\n", xl, fl, gl);
        }
        
        // Execute function, if experiment with this nb value is not present
        if (fabs(fr-0.0) <= eps) {

            // printf("Performing experiment with nb=%d\n", xr);

            (*dgetrf_time)(m, n, A, lda, iPiv,
                           xr, &fr, &gr);
            *exec += 1;

            // Store xr|fr|gr triplet in hash table
            hash_ins(h, xr, fr, gr);

            printf("%.d\t%.3f\t%.3f\n", xr, fr, gr);
        }

        plasma_time_t arr[] = {fa, fb, fm, fl, fr};

        plasma_time_t fmin = fmin_arr(arr, 5);
        // printf("fmin: %.3f\n", fmin);

        if ( (fabs(fmin-fa) <= eps) || (fabs(fmin-fl) <= eps) ) {
            // printf("fmin = fa OR fmin = fl\n");
            SHIFT2(xb,xm,xl); SHIFT2(fb,fm,fl);
        }
        else if (fabs(fmin-fm) <= eps) {
            // printf("fmin = fm\n");
            xa = xl; xb = xr; fa = fl; fb = fr;
        }
        else if ( (fabs(fmin-fr) <= eps) || (fabs(fmin-fb) <= eps) ) {
            // printf("fmin = fr OR fmin = fb\n");
            SHIFT2(xa,xm,xr); SHIFT2(fa,fm,fr);
        }

        *iter += 1;

        // printf("|xb-xa| ?> tol: %d\t%d\n", abs(xb-xa), *tol);

    } while (abs(xb-xa) > *tol);

    *nb = xm;

    // Print out full hash table
    // hash_print(h);

    // Save full hash table into file
    hash_save(h, fname, header);

    // Destroy hash table
    hash_free(h);

    return 0;
}


// Optimises DGEMM routine for best tile size 'nb' or no. of OpenMP threads 'nthr' using golden section algorithm
int goldsect_dgemm(int a, int b, int c, int *tol, int *nb, int *exec, int *iter,
                   char *fname, char *header,
                   plasma_enum_t transA, plasma_enum_t transB,
                   int m, int n, int k, double alpha, double *A, int lda, double *B,
                   int ldb, double beta, double *C, int ldc,
                   int (*dgemm_time)(plasma_enum_t, plasma_enum_t, int, int, int, double,
                                     double*, int, double*, int, double, double*, int,
                                     int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    double const P = 0.61803399;
    double const Q = 1.0-P;

    int    x0, x1, x2, x3;
    double f1, f2, g1, g2;

    *exec = 0;  *iter = 0;

    // Calculate size of hash table (no. of function executions)
    int const K = bisect_niter(a, c, *tol) + 2;

    // Create hash table to store nb|time value triplets
    hash_t *h = hash_new(K);

    // Bracket minimum
    bracket_min_dgemm(&a, &b, &c, tol, h, exec,
                      transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc,
                      (*dgemm_time));

    // hash_print(h);

    x0 = a;
    x3 = c;

    if (abs(c-b) > abs(b-a)) {
        x1 = b;
        x2 = nm(b+Q*(c-b), *tol);
    }
    else {
        x2 = b;
        x1 = nm(b-Q*(b-a), *tol);
    }

    // printf("%.d\t%.d\t%.d\n",      a,  b,  c);
    // printf("%.d\t%.d\t%.d\t%.d\n", x0, x1, x2, x3);

    // Execute function if experiment with this nb value is not present
    f1 = hash_lookup(h, x1, 't');

    if (fabs(f1-0.0) <= eps) {

        // printf("Performing experiment with nb=%d\n", x1);

        (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C,
                      ldc, x1, &f1, &g1);
        *exec += 1;

        // Store x1|f1|g1 triplet in hash table
        hash_ins(h, x1, f1, g1);

        printf("%.d\t%.3f\t%.3f\n", x1, f1, g1);
    }

    // Execute function if experiment with this nb value is not present
    f2 = hash_lookup(h, x2, 't');

    if (fabs(f2-0.0) <= eps) {

        // printf("Performing experiment with nb=%d\n", x2);

        (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C,
                      ldc, x2, &f2, &g2);
        *exec += 1;

        // Store x2|f2|g2 triplet in hash table
        hash_ins(h, x2, f2, g2);

        printf("%.d\t%.3f\t%.3f\n", x2, f2, g2);
    }

    while (abs(x3-x0) > *tol) {
        if (f2 < f1) {

            SHIFT3(x0, x1, x2, nm(P*x1+Q*x3,*tol));
            f1 = f2;

            // Execute function if experiment with this nb value is not present
            f2 = hash_lookup(h, x2, 't');

            if (fabs(f2-0.0) <= eps) {

                // printf("Performing experiment with nb=%d\n", x2);

                (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C,
                              ldc, x2, &f2, &g2);
                *exec += 1;

                // Store x2|f2|g2 triplet in hash table
                hash_ins(h, x2, f2, g2);

                printf("%.d\t%.3f\t%.3f\n", x2, f2, g2);
            }
        }
        else {

            SHIFT3(x3, x2, x1, nm(P*x2+Q*x0, *tol));
            f2 = f1;

            // Execute function if experiment with this nb value is not present
            f1 = hash_lookup(h, x1, 't');

            if (fabs(f1-0.0) < eps) {

                // printf("Performing experiment with nb=%d\n", x1);

                (*dgemm_time)(transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C,
                              ldc, x1, &f1, &g1);
                *exec += 1;

                // Store x1|f1|g1 triplet in hash table
                hash_ins(h, x1, f1, g1);

                printf("%.d\t%.3f\t%.3f\n", x1, f1, g1);
            }
        }

        // printf("x0, x1, x2, x3: %.d\t%.d\t%.d\t%.d\n", x0, x1, x2, x3);

        *iter += 1;
    }

    *nb = (f1 < f2) ? x1 : x2;

    // Print out full hash table
    // hash_print(h);

    // Save full hash table into file
    hash_save(h, fname, header);

    // Destroy hash table
    hash_free(h);

    return 0;
}


// Optimises DGEQRF routine for best tile size 'nb' or no. of OpenMP threads 'nthr' using golden section algorithm
int goldsect_dgeqrf(int a, int b, int c, int *tol, int *nb, int *exec, int *iter,
                    char *fname, char *header,
                    int m, int n, double *A, int lda, plasma_desc_t *T,
                    int (*dgeqrf_time)(int, int, double*, int, plasma_desc_t*,
                                       int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    double const P = 0.61803399;
    double const Q = 1.0-P;

    int    x0, x1, x2, x3;
    double f1, f2, g1, g2;

    *exec = 0;  *iter = 0;

    // Calculate size of hash table (no. of function executions)
    int const K = bisect_niter(a, c, *tol) + 2;

    // Create hash table to store nb|time value triplets
    hash_t *h = hash_new(K);

    // Bracket minimum
    bracket_min_dgeqrf(&a, &b, &c, tol, h, exec,
                       m, n, A, lda, T,
                      (*dgeqrf_time));

    // hash_print(h);

    x0 = a;
    x3 = c;

    if (abs(c-b) > abs(b-a)) {
        x1 = b;
        x2 = nm(b+Q*(c-b), *tol);
    }
    else {
        x2 = b;
        x1 = nm(b-Q*(b-a), *tol);
    }

    // printf("%.d\t%.d\t%.d\n",      a,  b,  c);
    // printf("%.d\t%.d\t%.d\t%.d\n", x0, x1, x2, x3);

    // Execute function if experiment with this nb value is not present
    f1 = hash_lookup(h, x1, 't');

    if (fabs(f1-0.0) <= eps) {

        // printf("Performing experiment with nb=%d\n", x1);

        (*dgeqrf_time)(m, n, A, lda, T,
                       x1, &f1, &g1);
        *exec += 1;

        // Store x1|f1|g1 triplet in hash table
        hash_ins(h, x1, f1, g1);

        printf("%.d\t%.3f\t%.3f\n", x1, f1, g1);
    }

    // Execute function if experiment with this nb value is not present
    f2 = hash_lookup(h, x2, 't');

    if (fabs(f2-0.0) <= eps) {

        // printf("Performing experiment with nb=%d\n", x2);

        (*dgeqrf_time)(m, n, A, lda, T,
                       x2, &f2, &g2);
        *exec += 1;

        // Store x2|f2|g2 triplet in hash table
        hash_ins(h, x2, f2, g2);

        printf("%.d\t%.3f\t%.3f\n", x2, f2, g2);
    }

    while (abs(x3-x0) > *tol) {
        if (f2 < f1) {

            SHIFT3(x0, x1, x2, nm(P*x1+Q*x3,*tol));
            f1 = f2;

            // Execute function if experiment with this nb value is not present
            f2 = hash_lookup(h, x2, 't');

            if (fabs(f2-0.0) <= eps) {

                // printf("Performing experiment with nb=%d\n", x2);

                (*dgeqrf_time)(m, n, A, lda, T,
                               x2, &f2, &g2);
                *exec += 1;

                // Store x2|f2|g2 triplet in hash table
                hash_ins(h, x2, f2, g2);

                printf("%.d\t%.3f\t%.3f\n", x2, f2, g2);
            }
        }
        else {

            SHIFT3(x3, x2, x1, nm(P*x2+Q*x0, *tol));
            f2 = f1;

            // Execute function if experiment with this nb value is not present
            f1 = hash_lookup(h, x1, 't');

            if (fabs(f1-0.0) < eps) {

                // printf("Performing experiment with nb=%d\n", x1);

                (*dgeqrf_time)(m, n, A, lda, T,
                               x1, &f1, &g1);
                *exec += 1;

                // Store x1|f1|g1 triplet in hash table
                hash_ins(h, x1, f1, g1);

                printf("%.d\t%.3f\t%.3f\n", x1, f1, g1);
            }
        }

        // printf("x0, x1, x2, x3: %.d\t%.d\t%.d\t%.d\n", x0, x1, x2, x3);

        *iter += 1;
    }

    *nb = (f1 < f2) ? x1 : x2;

    // Print out full hash table
    // hash_print(h);

    // Save full hash table into file
    hash_save(h, fname, header);

    // Destroy hash table
    hash_free(h);

    return 0;
}


// Optimises DGETRF routine for best tile size 'nb' or no. of OpenMP threads 'nthr' using golden section algorithm
int goldsect_dgetrf(int a, int b, int c, int *tol, int *nb, int *exec, int *iter,
                    char *fname, char *header,
                    int m, int n, double *A, int lda, int *iPiv,
                    int (*dgetrf_time)(int, int, double*, int, int*,
                                       int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    double const P = 0.61803399;
    double const Q = 1.0-P;

    int    x0, x1, x2, x3;
    double f1, f2, g1, g2;

    *exec = 0;  *iter = 0;

    // Calculate size of hash table (no. of function executions)
    int const K = bisect_niter(a, c, *tol) + 2;

    // Create hash table to store nb|time value triplets
    hash_t *h = hash_new(K);

    // Bracket minimum
    bracket_min_dgetrf(&a, &b, &c, tol, h, exec,
                       m, n, A, lda, iPiv,
                      (*dgetrf_time));

    // hash_print(h);

    x0 = a;
    x3 = c;

    if (abs(c-b) > abs(b-a)) {
        x1 = b;
        x2 = nm(b+Q*(c-b), *tol);
    }
    else {
        x2 = b;
        x1 = nm(b-Q*(b-a), *tol);
    }

    // printf("%.d\t%.d\t%.d\n",      a,  b,  c);
    // printf("%.d\t%.d\t%.d\t%.d\n", x0, x1, x2, x3);

    // Execute function if experiment with this nb value is not present
    f1 = hash_lookup(h, x1, 't');

    if (fabs(f1-0.0) <= eps) {

        // printf("Performing experiment with nb=%d\n", x1);

        (*dgetrf_time)(m, n, A, lda, iPiv,
                       x1, &f1, &g1);
        *exec += 1;

        // Store x1|f1|g1 triplet in hash table
        hash_ins(h, x1, f1, g1);

        printf("%.d\t%.3f\t%.3f\n", x1, f1, g1);
    }

    // Execute function if experiment with this nb value is not present
    f2 = hash_lookup(h, x2, 't');

    if (fabs(f2-0.0) <= eps) {

        // printf("Performing experiment with nb=%d\n", x2);

        (*dgetrf_time)(m, n, A, lda, iPiv,
                       x2, &f2, &g2);
        *exec += 1;

        // Store x2|f2|g2 triplet in hash table
        hash_ins(h, x2, f2, g2);

        printf("%.d\t%.3f\t%.3f\n", x2, f2, g2);
    }

    while (abs(x3-x0) > *tol) {
        if (f2 < f1) {

            SHIFT3(x0, x1, x2, nm(P*x1+Q*x3,*tol));
            f1 = f2;

            // Execute function if experiment with this nb value is not present
            f2 = hash_lookup(h, x2, 't');

            if (fabs(f2-0.0) <= eps) {

                // printf("Performing experiment with nb=%d\n", x2);

                (*dgetrf_time)(m, n, A, lda, iPiv,
                               x2, &f2, &g2);
                *exec += 1;

                // Store x2|f2|g2 triplet in hash table
                hash_ins(h, x2, f2, g2);

                printf("%.d\t%.3f\t%.3f\n", x2, f2, g2);
            }
        }
        else {

            SHIFT3(x3, x2, x1, nm(P*x2+Q*x0, *tol));
            f2 = f1;

            // Execute function if experiment with this nb value is not present
            f1 = hash_lookup(h, x1, 't');

            if (fabs(f1-0.0) < eps) {

                // printf("Performing experiment with nb=%d\n", x1);

                (*dgetrf_time)(m, n, A, lda, iPiv,
                               x1, &f1, &g1);
                *exec += 1;

                // Store x1|f1|g1 triplet in hash table
                hash_ins(h, x1, f1, g1);

                printf("%.d\t%.3f\t%.3f\n", x1, f1, g1);
            }
        }

        // printf("x0, x1, x2, x3: %.d\t%.d\t%.d\t%.d\n", x0, x1, x2, x3);

        *iter += 1;
    }

    *nb = (f1 < f2) ? x1 : x2;

    // Print out full hash table
    // hash_print(h);

    // Save full hash table into file
    hash_save(h, fname, header);

    // Destroy hash table
    hash_free(h);

    return 0;
}


// Optimises DPOTRF routine for best tile size 'nb' or no. of OpenMP threads 'nthr' using golden section algorithm
int goldsect_dpotrf(int a, int b, int c, int *tol, int *nb, int *exec, int *iter,
                    char *fname, char *header,
                    plasma_enum_t uplo, int n, double *A, int lda,
                    int (*dpotrf_time)(plasma_enum_t, int, double*, int,
                                       int, plasma_time_t*, double*)) {

    double eps = LAPACKE_dlamch('E');

    double const P = 0.61803399;
    double const Q = 1.0-P;

    int    x0, x1, x2, x3;
    double f1, f2, g1, g2;

    *exec = 0;  *iter = 0;

    // Calculate size of hash table (no. of function executions)
    int const K = bisect_niter(a, c, *tol) + 2;

    // Create hash table to store nb|time value triplets
    hash_t *h = hash_new(K);

    // Bracket minimum
    bracket_min_dpotrf(&a, &b, &c, tol, h, exec,
                       uplo, n, A, lda,
                      (*dpotrf_time));

    // hash_print(h);

    x0 = a;
    x3 = c;

    if (abs(c-b) > abs(b-a)) {
        x1 = b;
        x2 = nm(b+Q*(c-b), *tol);
    }
    else {
        x2 = b;
        x1 = nm(b-Q*(b-a), *tol);
    }

    // printf("%.d\t%.d\t%.d\n",      a,  b,  c);
    // printf("%.d\t%.d\t%.d\t%.d\n", x0, x1, x2, x3);

    // Execute function if experiment with this nb value is not present
    f1 = hash_lookup(h, x1, 't');

    if (fabs(f1-0.0) <= eps) {

        // printf("Performing experiment with nb=%d\n", x1);

        (*dpotrf_time)(uplo, n, A, lda,
                       x1, &f1, &g1);
        *exec += 1;

        // Store x1|f1|g1 triplet in hash table
        hash_ins(h, x1, f1, g1);

        printf("%.d\t%.3f\t%.3f\n", x1, f1, g1);
    }

    // Execute function if experiment with this nb value is not present
    f2 = hash_lookup(h, x2, 't');

    if (fabs(f2-0.0) <= eps) {

        // printf("Performing experiment with nb=%d\n", x2);

        (*dpotrf_time)(uplo, n, A, lda,
                       x2, &f2, &g2);
        *exec += 1;

        // Store x2|f2|g2 triplet in hash table
        hash_ins(h, x2, f2, g2);

        printf("%.d\t%.3f\t%.3f\n", x2, f2, g2);
    }

    while (abs(x3-x0) > *tol) {
        if (f2 < f1) {

            SHIFT3(x0, x1, x2, nm(P*x1+Q*x3,*tol));
            f1 = f2;

            // Execute function if experiment with this nb value is not present
            f2 = hash_lookup(h, x2, 't');

            if (fabs(f2-0.0) <= eps) {

                // printf("Performing experiment with nb=%d\n", x2);

                (*dpotrf_time)(uplo, n, A, lda,
                               x2, &f2, &g2);
                *exec += 1;

                // Store x2|f2|g2 triplet in hash table
                hash_ins(h, x2, f2, g2);

                printf("%.d\t%.3f\t%.3f\n", x2, f2, g2);
            }
        }
        else {

            SHIFT3(x3, x2, x1, nm(P*x2+Q*x0, *tol));
            f2 = f1;

            // Execute function if experiment with this nb value is not present
            f1 = hash_lookup(h, x1, 't');

            if (fabs(f1-0.0) < eps) {

                // printf("Performing experiment with nb=%d\n", x1);

                (*dpotrf_time)(uplo, n, A, lda,
                               x1, &f1, &g1);
                *exec += 1;

                // Store x1|f1|g1 triplet in hash table
                hash_ins(h, x1, f1, g1);

                printf("%.d\t%.3f\t%.3f\n", x1, f1, g1);
            }
        }

        // printf("x0, x1, x2, x3: %.d\t%.d\t%.d\t%.d\n", x0, x1, x2, x3);

        *iter += 1;
    }

    *nb = (f1 < f2) ? x1 : x2;

    // Print out full hash table
    // hash_print(h);

    // Save full hash table into file
    hash_save(h, fname, header);

    // Destroy hash table
    hash_free(h);

    return 0;
}

// EoF: bisect.c
