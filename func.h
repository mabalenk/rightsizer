/*
 * @brief Prototype rightsizer implementation.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 24, 2017
 * @version 0.15
 */

int function(double x, double y, double *f);


int dgemm_time_nb(plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                  double alpha, double *pA, int lda, double *pB, int ldb, double beta,
                  double *pC, int ldc, int nb, plasma_time_t *time, double *flops);

int dgeqrf_time_nb(int m, int n, double *pA, int lda, plasma_desc_t *T,
                   int nb, plasma_time_t *time, double *flops);

int dgetrf_time_nb(int m, int n, double *pA, int lda, int *iPiv,
                   int nb, plasma_time_t *time, double *flops);

int dpotrf_time_nb(plasma_enum_t uplo, int n, double *pA, int lda,
                   int nb, plasma_time_t *time, double *flops);


int dgemm_time_nthr(plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
                    double alpha, double *A, int lda, double *B, int ldb, double beta,
                    double *C, int ldc, int nthr, plasma_time_t *time, double *flops);

int dgeqrf_time_nthr(int m, int n, double *pA, int lda, plasma_desc_t *T,
                     int nthr, plasma_time_t *time, double *flops);

int dgetrf_time_nthr(int m, int n, double *pA, int lda, int *iPiv,
                     int nthr, plasma_time_t *time, double *flops);

int dpotrf_time_nthr(plasma_enum_t uplo, int n, double *A, int lda,
                     int nb, plasma_time_t *time, double *flops);


int dgemm_time(plasma_enum_t transA, plasma_enum_t transB, int m, int n, int k,
               double alpha, double *pA, int lda, double *pB, int ldb, double beta,
               double *pC, int ldc, int nb, int nthr, plasma_time_t *time, double *flops);

int dgeqrf_time(int m, int n, double *pA, int lda, plasma_desc_t *T,
                int nb, int nthr, plasma_time_t *time, double *flops);

int dgetrf_time(int m, int n, double *pA, int lda, int *iPiv,
                int nb, int nthr, plasma_time_t *time, double *flops);

int dpotrf_time(plasma_enum_t uplo, int n, double *pA, int lda,
                int nb, int nthr, plasma_time_t *time, double *flops);

// EoF: func.h
