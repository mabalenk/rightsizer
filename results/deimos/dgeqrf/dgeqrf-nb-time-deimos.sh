set terminal postscript eps enhanced colour solid 22;  # font "Helvetica, 22";
# set terminal png enhanced;

set lmargin 12;
set key top Left;
set format "%g";
set output "dgeqrf-nb-time-deimos.eps";
set title  "QR factorisation (DGEQRF) m=n=k=30000, n_{thr}=68";
set xlabel "Tile size, nb";
set ylabel "Execution time, t (s)";
set grid;
# set logscale x;

plot "dgeqrf.bisect.nb"   u ($1):($2) smooth unique t "Bisection"      w lp lw 5 ps 1.25, \
     "dgeqrf.goldsect.nb" u ($1):($2) smooth unique t "Golden section" w lp lw 5 ps 1.25;
