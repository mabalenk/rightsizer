/*
 * @brief Nelder-Mead optmisation method tester.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jun 30, 2017
 * @version 0.5
 */

#include "test.h"
#include "plasma.h"
#include "core_lapack.h"
#include "nelder_mead.h"
#include "func.h"

#include <stdio.h>


int main(int argc, char *argv[]) {

    // Vertices, initialise using compound literals
    vertex_t z1 = (vertex_t){.x = 0.0, .y = 0.0, .f = 0.0};
    vertex_t z2 = (vertex_t){.x = 1.2, .y = 0.0, .f = 0.0};
    vertex_t z3 = (vertex_t){.x = 0.0, .y = 0.8, .f = 0.0};
    
    int exec, iter;

    nelder_mead(&z1, &z2, &z3, &exec, &iter, function);

    printf("exec|iter: %d|%d\n", exec, iter);

    print_vertex("Optimal value pair", z1);

    return 0;
}

// EoF: test_nelder_mead.c
