/*
 * @brief Hash table tester.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 22, 2017
 * @version 0.5
 */

#include "hash_table.h"

#include <stdio.h>


int main(int argc, char *argv[]) {

    printf("Testing hash table functionality...\n");

    hash_t *h = hash_new(10);

    hash_ins(h,  64, 123.456, 200.123);
    hash_ins(h,  16, 213.456, 300.345);
    hash_ins(h,  32, 312.456, 400.567);
    hash_ins(h,   8, 456.456, 500.789);
    hash_ins(h,   4, 546.456, 600.098);
    hash_ins(h, 128, 645.456, 700.876);
    hash_ins(h,   2, 762.456, 800.543);
    hash_ins(h, 256, 267.456, 350.375);
    hash_ins(h, 512, 672.456, 750.274);
    hash_ins(h,  96, 627.456, 650.101);
    hash_print(h);

    hash_ins(h, 112, 531.097, 580.098);
    hash_print(h);

    hash_free(h);

    printf("Hash table test has been completed.\n");

    return 0;
}

// EoF: test_hash_table.c
