/*
 * @brief Implementation of basic hash table.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 22, 2017
 * @version 0.6
 */

// Hash table structure
typedef struct {
    int     n;
    int     size;
    int    *key;
    double *time;
    double *flops;
} hash_t;

hash_t *hash_new(int size);
void hash_free(hash_t *h);
int hash_idx(hash_t *h, int key);
void hash_print(hash_t *h);
void hash_save(hash_t *h, char *fname, char *header);
double hash_lookup(hash_t *h, int key, char type);
void hash_rehash(hash_t *h);
void hash_ins(hash_t *h, int key, double time, double flops);

// EoF: hash_table.h
