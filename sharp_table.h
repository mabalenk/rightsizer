/*
 * @brief Implementation of sharp table: table using hash function to create 'unique' key.
 *
 * @author  Maksims Abalenkovs
 * @email   m.abalenkovs@manchester.ac.uk
 * @date    Jul 22, 2017
 * @version 0.1
 */

// Sharp table structure
typedef struct {
    int     n;
    int     size;
    int    *key;
    int    *nb;
    int    *nthr;
    double *time;
    double *flops;
} sharp_t;

sharp_t *sharp_new(int size);
void sharp_free(sharp_t *s);
int gen_key(int nb, int nthr);
int sharp_idx(sharp_t *s, int nb, int nthr);
void sharp_print(sharp_t *s);
void sharp_save(sharp_t *s, char *fname, char *header);
double sharp_lookup(sharp_t *s, int nb, int nthr, char type);
void sharp_rehash(sharp_t *s);
void sharp_ins(sharp_t *s, int nb, int nthr, double time, double flops);

// EoF: sharp_table.h
