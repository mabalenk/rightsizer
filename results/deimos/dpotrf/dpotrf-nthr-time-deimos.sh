set terminal postscript eps enhanced colour solid 22;  # font "Helvetica, 22";
# set terminal png enhanced;

set lmargin 12;
set key top Left;
set format "%g";
set output "dpotrf-nthr-time-deimos.eps";
set title  "Cholesky factorisation (DPOTRF) m=n=k=30000, nb=448|480";
set xlabel "No. of threads, nthr";
set ylabel "Execution time, t (s)";
set grid;
# set logscale x;

plot "dpotrf.bisect.nthr"   u ($1):($2) smooth unique t "Bisection"      w lp lw 5 ps 1.25, \
     "dpotrf.goldsect.nthr" u ($1):($2) smooth unique t "Golden section" w lp lw 5 ps 1.25;
