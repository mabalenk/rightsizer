include make.inc

HDR = $(wildcard *.h)
SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)
# OBJ = $(filter-out rightsizer.o, $(SRC:.c=.o))
# EXE = $(OBJ:.o=)
# EXE = test_hash_table
# EXE = test_sharp_table
# EXE = test_bracket_min
# EXE = test_nelder_mead
EXE = rightsizer

$(EXE): $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $^ $(PLASMA_LIBS) $(LIBS)

%.o: %.c $(HDR)
	$(CC) $(CFLAGS) $(PLASMA_INC) $(INC) -c -o $@ $<

clean:
	rm -frv $(OBJ) $(EXE) *~

echo:
	@echo "SRC: $(SRC)"
	@echo "OBJ: $(OBJ)"
	@echo "EXE: $(EXE)"
